//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include <memory>
# include <vector>

# include "Model/Model.hpp"

# include "ModelInstances/___VARIABLE_groupName:identifier___/InputParameterList.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {


        class Model : public MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::/* choose your value among yes or no */>
        {
            
        private:
            
            //! \copydoc doxygen_hide_alias_self
            // \TODO This might seem a bit dumb but is actually very convenient for template classes.
            using self = Model;
            
            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::/* choose your value among yes or no */>;
                
        public:
            
            //! Return the name of the model.
            static const std::string& ClassName();
    
            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;
    
        public:
    
            /// \name Special members.
            ///@{
    
            /*! 
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);
            
            //! Destructor.
            ~Model() = default;
            
            //! Copy constructor.
            Model(const Model&) = delete;
            
            //! Move constructor.
            Model(Model&&) = delete;
            
            //! Copy affectation.
            Model& operator=(const Model&) = delete;
            
            //! Move affectation.
            Model& operator=(Model&&) = delete;
            
            ///@}
            
            
            /// \name Crtp-required methods.
            ///@{
          
           
           /*!
            * \brief Initialise the problem.
            *
            */
           void SupplInitialize();
           
               
           //! Manage time iteration.
           void Forward();
           
           /*!
            * \brief Additional operations to finalize a dynamic step.
            *
            * Base class already update the time for next time iterations.
            */
           void SupplFinalizeStep();
           
           
           /*!
            * \brief Initialise a dynamic step.
            *
            */
           void SupplFinalize();
           
                         
       private:
           
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
           bool SupplHasFinishedConditions() const;
           
           
           /*!
            * \brief Part of InitializedStep() specific to Elastic model.
            *
            * As there are none, the body of this method is empty.
            */
           void SupplInitializeStep();

           
           ///@}
            
    
        };
    
    
    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM


# include "ModelInstances/___VARIABLE_groupName:identifier___/___FILEBASENAME___.hxx"


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP) */
