//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "Core/InputParameter/Parameter/___FILEBASENAMEASIDENTIFIER___.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
             
        
        const std::string& ___FILEBASENAMEASIDENTIFIER___::GetName()
        {
            static std::string ret("___FILEBASENAMEASIDENTIFIER___");
            return ret;
        };
        
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM
