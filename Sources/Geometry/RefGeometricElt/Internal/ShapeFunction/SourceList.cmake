target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AccessShapeFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AccessShapeFunction.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Alias.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ConstantShapeFunction.hpp"
)

