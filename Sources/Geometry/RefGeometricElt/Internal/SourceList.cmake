target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/RefGeomElt/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
