///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Mar 2014 16:15:09 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle8.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{
    
    
    namespace RefGeomEltNS
    {
        
        
        namespace ShapeFunctionNS
        {
            
            
            const std::array<ShapeFunctionType, 8>& Quadrangle8::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 8> ret
                {
                    {
                        [](const auto& local_coords) { return 0.25 * (-1. - local_coords.r() - local_coords.s()) * (1. - local_coords.r()) * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (-1. + local_coords.r() - local_coords.s()) * (1. + local_coords.r()) * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (-1. + local_coords.r() + local_coords.s()) * (1. + local_coords.r()) * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (-1. - local_coords.r() + local_coords.s()) * (1. - local_coords.r()) * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. - (local_coords.r() * local_coords.r())) * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()) * (1. - (local_coords.s() * local_coords.s())); },
                        [](const auto& local_coords) { return 0.5 * (1. - (local_coords.r() * local_coords.r())) * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r()) * (1. - (local_coords.s() * local_coords.s())); }
                    }
                };
                
                return ret;
            };
            
            
            const std::array<ShapeFunctionType, 16>& Quadrangle8::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 16> ret
                {
                    {
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.s()) * (-2. * local_coords.r() + local_coords.s()); },
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.r()) * (-local_coords.r() - 2. * local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (1. - local_coords.s()) * (2. * local_coords.r() + local_coords.s()); },
                        [](const auto& local_coords) { return -0.25 * (1. + local_coords.r()) * (local_coords.r() - 2. * local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.s()) * (2. * local_coords.r() - local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.r()) * (local_coords.r() + 2. * local_coords.s()); },
                        [](const auto& local_coords) { return -0.25 * (1. + local_coords.s()) * (-2. * local_coords.r() - local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (1. - local_coords.r()) * (-local_coords.r() + 2. * local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.r() * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r() * local_coords.r()); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.s() * (1. + local_coords.r()); },
                        [](const auto& local_coords) { return -local_coords.r() * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r() * local_coords.r()); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.s() * (1. - local_coords.r()); }
                    }
                };
                
                return ret;
            };
            
            
            
            const std::array<ShapeFunctionType, 32>& Quadrangle8::SecondDerivateShapeFunctionList()
            {
                
                static std::array<ShapeFunctionType, 32> ret
                {
                    {
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (-2. * local_coords.r() + 2. * local_coords.s() - 1.); },
                        [](const auto& local_coords) { return 0.25 * (-2. * local_coords.r() - 2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r()); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return -0.25 * (2. * local_coords.r() + 2. * local_coords.s() - 1.); },
                        [](const auto& local_coords) { return -0.25 * (2. * local_coords.r() - 2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.25 * (2. * local_coords.r() - 2. * local_coords.s() - 1.); },
                        [](const auto& local_coords) { return 0.25 * (2. * local_coords.r() + 2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return -0.25 * (-2. * local_coords.r() - 2. * local_coords.s() - 1.); },
                        [](const auto& local_coords) { return -0.25 * (-2. * local_coords.r() + 2. * local_coords.s() +1.); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r()); },
                        
                        [](const auto& local_coords) { return -1. + local_coords.s(); },
                        [](const auto& local_coords) { return local_coords.r(); },
                        [](const auto& local_coords) { return local_coords.r(); },
                        Constant<0>(),
                        
                        Constant<0>(),
                        [](const auto& local_coords) {  return -local_coords.s(); },
                        [](const auto& local_coords) { return -local_coords.s(); },
                        [](const auto& local_coords) { return -1. - local_coords.r(); },
                        
                        [](const auto& local_coords) { return -1. - local_coords.s(); },
                        [](const auto& local_coords) { return -local_coords.r(); },
                        [](const auto& local_coords) { return -local_coords.r(); },
                        Constant<0>(),
                        
                        Constant<0>(),
                        [](const auto& local_coords) { return local_coords.s(); }, 
                        [](const auto& local_coords) { return local_coords.s(); }, 
                        [](const auto& local_coords) { return -1. + local_coords.r(); }
                    }
                };
                
                return ret;
            };
            
            
        } //  namespace ShapeFunctionNS
        
        
    } // namespace RefGeomEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
