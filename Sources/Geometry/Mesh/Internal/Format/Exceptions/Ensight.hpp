///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_


# include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            namespace Ensight
            {



                /*!
                 * \brief Thrown if third or fourth line is not what is expected
                 *
                 * Expected format is "XXX id YYY" where XXX is 'node' or 'element' and YYY is among
                 * <off/given/assign/ignore>
                 */
                class InvalidThirdOrFourthLine final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] line_number 3 or 4
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidThirdOrFourthLine(const std::string& ensight_file,
                                                      unsigned int line_number,
                                                      const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~InvalidThirdOrFourthLine();

                    //! Copy constructor.
                    InvalidThirdOrFourthLine(const InvalidThirdOrFourthLine&) = default;

                    //! Move constructor.
                    InvalidThirdOrFourthLine(InvalidThirdOrFourthLine&&) = default;

                    //! Copy affectation.
                    InvalidThirdOrFourthLine& operator=(const InvalidThirdOrFourthLine&) = default;

                    //! Move affectation.
                    InvalidThirdOrFourthLine& operator=(InvalidThirdOrFourthLine&&) = default;

                };



                /*!
                 * \brief Thrown when the announced number of coords has not been retrieved.
                 */
                class InvalidCoords final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] expected_number Number of coords plainly written in the Ensight file
                     * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidCoords(const std::string& ensight_file,
                                           unsigned int expected_number,
                                           unsigned int number_found,
                                           const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~InvalidCoords();

                    //! Copy constructor.
                    InvalidCoords(const InvalidCoords&) = default;

                    //! Move constructor.
                    InvalidCoords(InvalidCoords&&) = default;

                    //! Copy affectation.
                    InvalidCoords& operator=(const InvalidCoords&) = default;

                    //! Move affectation.
                    InvalidCoords& operator=(InvalidCoords&&) = default;


                };

                /*!
                 * \brief Thrown when the announced number of geometric elements has not been retrieved.
                 */
                class BadNumberOfEltsInLabel final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] index Index of the block considered (the 'part ###' bit in the Ensign=ht file)
                     * \param[in] description Name associated to the label considered.
                     * \param[in] expected_number Number of coords plainly written in the Ensight file
                     * \param[in] geometric_elt_name Name of the geometric element being currently read.
                     * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit BadNumberOfEltsInLabel(const std::string& ensight_file,
                                                    unsigned int index,
                                                    const std::string& description,
                                                    const std::string& geometric_elt_name,
                                                    unsigned int expected_number,
                                                    unsigned int number_found,
                                                    const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~BadNumberOfEltsInLabel();

                    //! Copy constructor.
                    BadNumberOfEltsInLabel(const BadNumberOfEltsInLabel&) = default;

                    //! Move constructor.
                    BadNumberOfEltsInLabel(BadNumberOfEltsInLabel&&) = default;

                    //! Copy affectation.
                    BadNumberOfEltsInLabel& operator=(const BadNumberOfEltsInLabel&) = default;

                    //! Move affectation.
                    BadNumberOfEltsInLabel& operator=(BadNumberOfEltsInLabel&&) = default;


                };



                /*!
                 * \brief Thrown when a block doesn't begin with the expected string
                 */
                class InvalidLabelBlock final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] ensight_file Ensight file being read\
                     * \param[in] stringRead String read where "part" was expected
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidLabelBlock(const std::string& ensight_file,
                                               const std::string& stringRead,
                                               const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~InvalidLabelBlock();

                    //! Copy constructor.
                    InvalidLabelBlock(const InvalidLabelBlock&) = default;

                    //! Move constructor.
                    InvalidLabelBlock(InvalidLabelBlock&&) = default;

                    //! Copy affectation.
                    InvalidLabelBlock& operator=(const InvalidLabelBlock&) = default;

                    //! Move affectation.
                    InvalidLabelBlock& operator=(InvalidLabelBlock&&) = default;

                };


                /*!
                 * \brief Thrown when number of geometric elements can't be read properly
                 */
                class InvalidNumberOfGeometricElts final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidNumberOfGeometricElts(const std::string& ensight_file,
                                                          const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~InvalidNumberOfGeometricElts();

                    //! Copy constructor.
                    InvalidNumberOfGeometricElts(const InvalidNumberOfGeometricElts&) = default;

                    //! Move constructor.
                    InvalidNumberOfGeometricElts(InvalidNumberOfGeometricElts&&) = default;

                    //! Copy affectation.
                    InvalidNumberOfGeometricElts& operator=(const InvalidNumberOfGeometricElts&) = default;

                    //! Move affectation.
                    InvalidNumberOfGeometricElts& operator=(InvalidNumberOfGeometricElts&&) = default;


                };


            } // namespace Ensight


        } // namespace Format


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_
