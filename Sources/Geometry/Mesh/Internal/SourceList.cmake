target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MeshManager.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MeshManager.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MeshManager.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
