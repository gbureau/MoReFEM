///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 16:11:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_


namespace MoReFEM
{


    template<class Type2TypeTopologyT>
    OrientedEdge::OrientedEdge(const Edge::shared_ptr& edge_ptr,
                               const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                               unsigned int local_edge_index,
                               Type2TypeTopologyT topology_token)
    : Crtp::Orientation<OrientedEdge, Edge>(edge_ptr,
                                            Internal::InterfaceNS::ComputeEdgeOrientation<typename Type2TypeTopologyT::type>(coords_list_in_geom_elt,
                                                                                                                            local_edge_index))
    {
        static_cast<void>(topology_token);
        assert(GetOrientation() < 2u && "2 possible values for edge orientation!");
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_
