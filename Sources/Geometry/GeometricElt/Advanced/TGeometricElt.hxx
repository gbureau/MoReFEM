///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 14:21:56 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GeomEltNS
        {


/// Deactivate noreturn here: it would trigger false positives. )
PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id)
            : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            unsigned int index,
                                                            std::vector<unsigned int>&& coords)
            : GeometricElt(mesh_unique_id, mesh_coords_list, index, std::move(coords))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            std::vector<unsigned int>&& coords)
            : GeometricElt(mesh_unique_id, mesh_coords_list, std::move(coords))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(unsigned int mesh_unique_id,
                                                            const Coords::vector_unique_ptr& mesh_coords_list,
                                                            std::istream& stream)
            : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();

                std::vector<unsigned int> coords_list(TraitsRefGeomEltT::Ncoords);

                for (unsigned int i = 0u; i < TraitsRefGeomEltT::Ncoords; ++i)
                    stream >> coords_list[i];

                if (stream)
                {
                    // Modify geometric element only if failbit not set.
                    SetCoordsList(mesh_coords_list, std::move(coords_list));
                }
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::~TGeometricElt() = default;


            template<class TraitsRefGeomEltT>
            Advanced::GeometricEltEnum TGeometricElt<TraitsRefGeomEltT>::GetIdentifier() const
            {
                return TraitsRefGeomEltT::Identifier();
            }


            template<class TraitsRefGeomEltT>
            const std::string TGeometricElt<TraitsRefGeomEltT>::GetName() const
            {
                return TraitsRefGeomEltT::ClassName();
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Ncoords() const
            {
                return static_cast<unsigned int>(TraitsRefGeomEltT::Ncoords);
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nvertex() const
            {
                return TraitsRefGeomEltT::topology::Nvertex;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nedge() const
            {
                return TraitsRefGeomEltT::topology::Nedge;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::Nface() const
            {
                return TraitsRefGeomEltT::topology::Nface;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TGeometricElt<TraitsRefGeomEltT>::GetDimension() const
            {
                return TraitsRefGeomEltT::topology::dimension;
            }


            template<class TraitsRefGeomEltT>
            inline const std::string& TGeometricElt<TraitsRefGeomEltT>::GetEnsightName() const
            {
                static auto ret = Internal::MeshNS::FormatNS::Ensight::Dispatch::GetName<TraitsRefGeomEltT>(EnsightSupport());
                return ret;
            }


            template<class TraitsRefGeomEltT>
            inline GmfKwdCod TGeometricElt<TraitsRefGeomEltT>::GetMeditIdentifier() const
            {
                return Internal::MeshNS::FormatNS::Medit::Dispatch::GetIdentifier<TraitsRefGeomEltT>(MeditSupport());
            }


            template<class TraitsRefGeomEltT>
            inline void TGeometricElt<TraitsRefGeomEltT>::WriteEnsightFormat(std::ostream& stream, bool do_print_index) const
            {
                Internal::MeshNS::FormatNS::Ensight::Dispatch::WriteFormat
                <
                    TraitsRefGeomEltT,
                    TraitsRefGeomEltT::Ncoords
                >(EnsightSupport(),
                  stream,
                  do_print_index,
                  GetIndex(),
                  GetCoordsList());
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>
            ::WriteMeditFormat(const int mesh_index,
                               const std::unordered_map<unsigned int, int>& processor_wise_reindexing) const
            {
                auto label_ptr = GetMeshLabelPtr();
                int label_index = (label_ptr ? static_cast<int>(label_ptr->GetIndex()) : 0);

                const auto& coords_list = GetCoordsList();

                // Medit assumes indexes between 1 and Ncoord, whereas MoReFEM starts at 0.
                std::vector<int> coord_index_list;
                coord_index_list.reserve(coords_list.size());

                for (const auto& coord_ptr : coords_list)
                {
                    assert(!(!coord_ptr));
                    auto it = processor_wise_reindexing.find(coord_ptr->GetIndex());

                    assert(it != processor_wise_reindexing.cend() && "Otherwise processor_wise_reindexing is poorly built.");

                    coord_index_list.push_back(it->second);
                }

                Internal::MeshNS::FormatNS::Medit::Dispatch::WriteFormat
                <
                    TraitsRefGeomEltT,
                    TraitsRefGeomEltT::Ncoords
                >(MeditSupport(),
                  mesh_index,
                  GetMeditIdentifier(),
                  coord_index_list,
                  label_index);
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::ReadMeditFormat(const Coords::vector_unique_ptr& mesh_coords_list,
                                                                   int libmesh_mesh_index,
                                                                   unsigned int Ncoord_in_mesh,
                                                                   int& label_index)
            {
                std::vector<unsigned int> coords(TraitsRefGeomEltT::Ncoords);

                Internal::MeshNS::FormatNS::Medit::Dispatch::ReadFormat
                <
                    TraitsRefGeomEltT,
                    TraitsRefGeomEltT::Ncoords
                >(MeditSupport(),
                  libmesh_mesh_index,
                  GetMeditIdentifier(),
                  coords,
                  label_index);

                // Check here the indexes of the coords are between 1 and Ncoord:
                for (auto coord_index : coords)
                {
                    if (coord_index == 0 || coord_index > Ncoord_in_mesh)
                        throw MoReFEM::ExceptionNS::Format::Medit::InvalidCoordIndex(TraitsRefGeomEltT::ClassName(),
                                                                                        coord_index,
                                                                                        Ncoord_in_mesh,
                                                                                        __FILE__,
                                                                                        __LINE__);
                }

                SetCoordsList(mesh_coords_list, std::move(coords));
            }



            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>::ShapeFunction(unsigned int i,
                                                                             const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::ShapeFunction(i, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>
            ::FirstDerivateShapeFunction(unsigned int i, unsigned int icoor,
                                         const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::FirstDerivateShapeFunction(i, icoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>
            ::SecondDerivateShapeFunction(unsigned int i,
                                          unsigned int icoor,
                                          unsigned int jcoor,
                                          const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::SecondDerivateShapeFunction(i, icoor, jcoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildVertexList(const GeometricElt* geom_elt_ptr,
                            Vertex::InterfaceMap& existing_list)
            {
                const auto& coords_list = GetCoordsList();

                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& vertex_list =
                    Internal::InterfaceNS::Build<Vertex, Topology>::Perform(geom_elt_ptr, coords_list, existing_list);

                SetVertexList(std::move(vertex_list));
            }



            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildEdgeList(const GeometricElt* geom_elt_ptr,
                          Edge::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                constexpr bool is_edge = (Topology::Nedge > 0u);

                auto&& oriented_edge_list =
                    Internal::InterfaceNS::BuildEdgeListHelper<Topology, is_edge>::Perform(geom_elt_ptr,
                                                                                          GetCoordsList(),
                                                                                          existing_list);

                SetOrientedEdgeList(std::move(oriented_edge_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildFaceList(const GeometricElt* geom_elt_ptr,
                          Face::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                constexpr bool is_face = (Topology::Nface > 0u);

                auto&& oriented_face_list =
                    Internal::InterfaceNS::BuildFaceListHelper<Topology, is_face>::Perform(geom_elt_ptr,
                                                                                          GetCoordsList(),
                                                                                          existing_list);

                SetOrientedFaceList(std::move(oriented_face_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::
            BuildVolumeList(const GeometricElt* geom_elt,
                            Volume::InterfaceMap& existing_list)
            {
                Volume::shared_ptr volume_ptr =
                (TraitsRefGeomEltT::topology::Nvolume > 0u ? std::make_shared<Volume>(shared_from_this()) : nullptr);
                SetVolume(volume_ptr);

                if (!(!volume_ptr))
                {
                    assert("One Volume shoudn't be entered twice in the list: it is proper to each GeometricElt."
                           && existing_list.find(volume_ptr) == existing_list.cend());

                    std::vector<const GeometricElt*> vector_raw;
                    vector_raw.push_back(geom_elt);

                    existing_list.insert({volume_ptr, vector_raw});
                }

            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::StaticAssertBuiltInConsistency()
            {
                static_assert(static_cast<unsigned int>(TraitsRefGeomEltT::Nderivate_component_) ==
                              static_cast<unsigned int>(TraitsRefGeomEltT::topology::dimension),
                              "Shape function should be consistent with geometric element!");
            }


// Reactivate the warning disabled at the beginning of this file.
PRAGMA_DIAGNOSTIC(pop)


        } // namespace GeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup




#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
