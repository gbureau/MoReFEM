///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_



# include "Utilities/Exceptions/Factory.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"



namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Factory
        {


            // Use some of the Factory Exception classes already defined in Utilities.
            using ExceptionNS::Factory::Exception;
            using ExceptionNS::Factory::UnableToRegister;


            namespace GeometricElt
            {


                //! Generic exception for GeometricEltFactory.
                class Exception : public Factory::Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message.
                     *
                     * \param[in] msg Message
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                    //! Destructor.
                    virtual ~Exception() override;

                    //! Copy constructor.
                    Exception(const Exception&) = default;

                    //! Move constructor.
                    Exception(Exception&&) = default;

                    //! Copy affectation.
                    Exception& operator=(const Exception&) = default;

                    //! Move affectation.
                    Exception& operator=(Exception&&) = default;

                };



                //! Thrown when the user requires from the factory an object with an unknown Ensight name (which acts as an identifier).
                class InvalidEnsightGeometricEltName final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] ensight_name Attempted Ensight name of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidEnsightGeometricEltName(const std::string& ensight_name,
                                                            const char* invoking_file, int invoking_line);

                    //! Destructor.
                    virtual ~InvalidEnsightGeometricEltName();

                    //! Copy constructor.
                    InvalidEnsightGeometricEltName(const InvalidEnsightGeometricEltName&) = default;

                    //! Move constructor.
                    InvalidEnsightGeometricEltName(InvalidEnsightGeometricEltName&&) = default;

                    //! Copy affectation.
                    InvalidEnsightGeometricEltName& operator=(const InvalidEnsightGeometricEltName&) = default;

                    //! Move affectation.
                    InvalidEnsightGeometricEltName& operator=(InvalidEnsightGeometricEltName&&) = default;


                };



                /*!
                 * \brief Thrown when the user requires from the factory an object with an unknown identifier.
                 *
                 * Said identifier is one given by the class itself and is not related to a specific IO format.
                 */
                class InvalidGeometricEltId final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] identifier Attempted identifier of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidGeometricEltId(Advanced::GeometricEltEnum identifier,
                                                   const char* invoking_file, int invoking_line);

                    //! Destructor.
                    virtual ~InvalidGeometricEltId();

                    //! Copy constructor.
                    InvalidGeometricEltId(const InvalidGeometricEltId&) = default;

                    //! Move constructor.
                    InvalidGeometricEltId(InvalidGeometricEltId&&) = default;

                    //! Copy affectation.
                    InvalidGeometricEltId& operator=(const InvalidGeometricEltId&) = default;

                    //! Move affectation.
                    InvalidGeometricEltId& operator=(InvalidGeometricEltId&&) = default;


                };


                /*!
                 * \brief Thrown when the user requires from the factory an object with an unknown name.
                 *
                 * Said identifier is one given by the class itself and is not related to a specific IO format.
                 */
                class InvalidGeometricEltName final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] ref_geom_elt_name Attempted name of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidGeometricEltName(const std::string& ref_geom_elt_name,
                                                     const char* invoking_file, int invoking_line);

                    //! Destructor.
                    virtual ~InvalidGeometricEltName();

                    //! Copy constructor.
                    InvalidGeometricEltName(const InvalidGeometricEltName&) = default;

                    //! Move constructor.
                    InvalidGeometricEltName(InvalidGeometricEltName&&) = default;

                    //! Copy affectation.
                    InvalidGeometricEltName& operator=(const InvalidGeometricEltName&) = default;

                    //! Move affectation.
                    InvalidGeometricEltName& operator=(InvalidGeometricEltName&&) = default;


                };




            } // namespace GeometricElt


        } // namespace Factory


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_
