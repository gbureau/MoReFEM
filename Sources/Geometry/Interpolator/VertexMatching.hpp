///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HPP_

# include <memory>
# include <vector>
# include <unordered_map>

# include "Utilities/InputParameterList/Base.hpp"

# include "Core/InputParameter/Geometry/InterpolationFile.hpp"

# include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace NonConformInterpolatorNS
    {


        class FromVertexMatching;


    } // namespace NonConformInterpolatorNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace MeshNS
    {


        namespace InterpolationNS
        {


            /*!
             * \brief Interpolation berween vertices of two meshes as given by an ad hoc file.
             *
             * This class is built upon the data read in the input parameter file, and is used only within
             * FromVertexMatching interpolator.
             *
             * \attention Works only for P1 geometry!
             */
            class VertexMatching
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = VertexMatching;

                //! Frienship to the class that actually requires vertex matching informations.
                friend class NonConformInterpolatorNS::FromVertexMatching;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor from an input parameter file.
                 *
                 * \copydoc doxygen_hide_input_parameter_data_arg
                 */
                template<class InputParameterDataT>
                explicit VertexMatching(const InputParameterDataT& input_parameter_data);

                //! Destructor.
                ~VertexMatching() = default;

                //! Copy constructor.
                VertexMatching(const VertexMatching&) = delete;

                //! Move constructor.
                VertexMatching(VertexMatching&&) = delete;

                //! Copy affectation.
                VertexMatching& operator=(const VertexMatching&) = delete;

                //! Move affectation.
                VertexMatching& operator=(VertexMatching&&) = delete;

                ///@}


                //! Returns the source index that matches the given \a target_index.
                unsigned int FindSourceIndex(unsigned int target_index) const;


            private:

                //! Get the list of source indexes.
                const std::vector<unsigned int>& GetSourceIndexList() const noexcept;

                //! Get the list of target indexes.
                const std::vector<unsigned int>& GetTargetIndexList() const noexcept;

                //! Method that does the bulk of constructor job. Should not be called out of constructor.
                void Read(const std::string& filename);

            private:

                //! Relationship between vertices of both meshes.
                std::vector<unsigned int> source_index_list_;

                //! Must be the same size as \a source_index_list_.
                std::vector<unsigned int> target_index_list_;
            };


        } // namespace InterpolationNS



    } // namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interpolator/VertexMatching.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_VERTEX_MATCHING_HPP_
