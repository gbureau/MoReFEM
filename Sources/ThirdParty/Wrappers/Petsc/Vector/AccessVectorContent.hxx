///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Jul 2014 15:37:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {



            template<Utilities::Access AccessT>
            inline typename VectorForAccess<AccessT>::scalar_array_type AccessVectorContent<AccessT>::GetArray() const
            {
                return values_;
            }


            template<Utilities::Access AccessT>
            inline PetscScalar AccessVectorContent<AccessT>::GetValue(unsigned int i) const
            {
                assert(values_ != NULL);
                assert(i <= static_cast<unsigned int>(vector_.GetProcessorWiseSize(__FILE__, __LINE__)));
                return values_[i];
            }


            template<Utilities::Access AccessT>
            inline unsigned int AccessVectorContent<AccessT>::GetSize(const char* invoking_file, int invoking_line) const
            {
                assert(vector_.InternalWithoutCheck() != NULL);
                return static_cast<unsigned int>(vector_.GetProcessorWiseSize(invoking_file, invoking_line));
            }


            template<Utilities::Access AccessT>
            PetscScalar& AccessVectorContent<AccessT>::operator[](unsigned int i)
            {
                static_assert(AccessT != Utilities::Access::read_only,
                              "This method should only be called when read/write rights are given!");
                assert(values_ != NULL);
                assert(i <= static_cast<unsigned int>(vector_.GetProcessorWiseSize(__FILE__, __LINE__)));
                return values_[i];
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_
