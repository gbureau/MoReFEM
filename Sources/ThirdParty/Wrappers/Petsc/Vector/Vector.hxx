///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:00:51 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            inline Vec Vector::Internal() const
            {
                assert(petsc_vector_ != PETSC_NULL);
                return petsc_vector_;
            }


            inline Vec Vector::InternalWithoutCheck() const
            {
                return petsc_vector_;
            }


            template<Utilities::Access AccessT>
            inline void Vector::SetValues(const std::vector<PetscInt>& indexing,
                                          const AccessVectorContent<AccessT>& local_vec,
                                          InsertMode insertOrAppend, const char* invoking_file, int invoking_line)
            {
                this->SetValues(indexing, local_vec.GetArray(), insertOrAppend, invoking_file, invoking_line);
            }


            template<MpiScale MpiScaleT>
            void Vector::Print(const Mpi& mpi,
                               const std::string& output_file,
                               const char* invoking_file, int invoking_line) const
            {
                switch(MpiScaleT)
                {
                    case MpiScale::program_wise:
                        View(mpi, output_file, invoking_file, invoking_line, PETSC_VIEWER_ASCII_MATLAB);
                        break;
                    case MpiScale::processor_wise:
                        Internal::Wrappers::Petsc::PrintPerProcessor(*this, output_file, invoking_file, invoking_line);
                        break;
                }
            }


            inline void Vector::UpdateGhosts(const char* invoking_file, int invoking_line,
                                             update_ghost do_update_ghost)
            {
                switch(do_update_ghost)
                {
                    case update_ghost::yes:
                        UpdateGhosts(invoking_file, invoking_line);
                        break;
                    case update_ghost::no:
                        break;
                }
            }




        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup



#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_
