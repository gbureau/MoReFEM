///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 18:06:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HPP_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HPP_

# include <cassert>

# include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            /*!
             * \brief A convenient class which holds relevant data to perform a conform interpolation.
             */
            class InterpolationData
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = InterpolationData;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit InterpolationData(SourceOrTargetData::unique_ptr&& source_data,
                                           SourceOrTargetData::unique_ptr&& target_data);

                //! Destructor.
                ~InterpolationData() = default;

                //! Copy constructor.
                InterpolationData(const InterpolationData&) = delete;

                //! Move constructor.
                InterpolationData(InterpolationData&&) = delete;

                //! Copy affectation.
                InterpolationData& operator=(const InterpolationData&) = delete;

                //! Move affectation.
                InterpolationData& operator=(InterpolationData&&) = delete;

                ///@}

            public:

                //! Accessor to source data.
                const SourceOrTargetData& GetSourceData() const noexcept;

                //! Accessor to target data.
                const SourceOrTargetData& GetTargetData() const noexcept;

            private:

                //! Object which hols all relevant informations about the source.
                SourceOrTargetData::unique_ptr source_data_ = nullptr;

                //! Object which hols all relevant informations about the target.
                SourceOrTargetData::unique_ptr target_data_ = nullptr;
            };


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ConformInterpolator/Advanced/InterpolationData.hxx"


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HPP_
