///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:41:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            # ifndef NDEBUG


            # endif // NDEBUG


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HXX_
