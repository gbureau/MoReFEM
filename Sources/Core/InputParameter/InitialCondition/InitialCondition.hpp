///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_

# include <memory>
# include <vector>


# include "Core/InputParameter/Crtp/Section.hpp"
# include "Core/InputParameter/InitialCondition/Impl/InitialCondition.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct InitialCondition: public Crtp::Section<InitialCondition<IndexT>, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = InitialCondition<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
             */
            struct Nature
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Nature, self, InitialConditionNS::Nature::storage_type>,
            public InitialConditionNS::Nature
            { };

            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Scalar, self, InitialConditionNS::Scalar::storage_type>,
            public InitialConditionNS::Scalar
            { };


            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionX
            : public Crtp::InputParameter<LuaFunctionX, self, InitialConditionNS::LuaFunctionX::storage_type>,
            public InitialConditionNS::LuaFunctionX
            { };


            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionY
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<LuaFunctionY, self, InitialConditionNS::LuaFunctionY::storage_type>,
            public InitialConditionNS::LuaFunctionY
            { };



            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionZ
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<LuaFunctionZ, self, InitialConditionNS::LuaFunctionZ::storage_type>,
            public InitialConditionNS::LuaFunctionZ
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Nature,
                Scalar,
                LuaFunctionX,
                LuaFunctionY,
                LuaFunctionZ
            >;

        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct InitialCondition


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

# include "Core/InputParameter/InitialCondition/InitialCondition.hxx"

#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
