///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Dec 2015 16:45:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_INTERPOLATION_FILE_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_INTERPOLATION_FILE_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {



    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_INTERPOLATION_FILE_HXX_
