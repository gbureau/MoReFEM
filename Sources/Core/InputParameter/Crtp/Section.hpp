///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 14:08:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HPP_

# include <string>

# include "Utilities/InputParameterList/Crtp/Section.hpp"
# include "Utilities/InputParameterList/Crtp/InputParameter.hpp"



namespace MoReFEM
{


    namespace InputParameter
    {


        //! Convenient alias when a parameter or a section is not enclosed in a section.
        using NoEnclosingSection = Utilities::InputParameterListNS::NoEnclosingSection;


        namespace Crtp
        {


            /*!
             * \brief Convenient alias for a Section.
             *
             * \tparam DerivedT Placeholder for the class upon which Crtp is applied.
             * \tparam EnclosingSectionT Type of the section that enclose current section. Might be NoEnclosingSection.
             */
            template<class DerivedT, class EnclosingSectionT>
            using Section = Utilities::InputParameterListNS::Crtp::Section<DerivedT, EnclosingSectionT>;


            /*!
             * \brief Convenient alias for an input parameter..
             *
             * \tparam DerivedT Placeholder for the class upon which Crtp is applied.
             * \tparam EnclosingSectionT Type of the section that enclose current section. Might be NoEnclosingSection.
             * \tparam ReturnTypeT Return type of the \a InputParameter.
             */
            template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
            using InputParameter = Utilities::InputParameterListNS::Crtp::InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>;



        } // namespace Crtp


        namespace Impl
        {


            /*!
             * \brief Generates the name of the templatized section.
             *
             * \param[in] name Name of the type of section looked at. For instance 'Mesh', 'Domain'.
             * \param[in] index Index related to the section. For instance index = 1 will yield 'Foo1' if \a name is 'Foo'.
             *
             * \return Generated name.
             */
            std::string GenerateSectionName(std::string&& name, unsigned int index);



        } // namespace Impl


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Crtp/Section.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_CRTP_x_SECTION_HPP_
