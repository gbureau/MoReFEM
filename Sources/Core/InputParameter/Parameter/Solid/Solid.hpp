///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:36:46 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOLID_x_SOLID_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOLID_x_SOLID_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"
# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"
# include "Core/InputParameter/Parameter/MaterialProperty/VolumicMass.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        /*!
         * \copydoc doxygen_hide_core_input_parameter_list_section
         *
         * Solid encompasses a lot of parameters; it is likely all of them are not required in the model you intend
         * to write. So you have two options:
         * - Either put InputParameter::Solid in your input parameter tuple; in this case all of them will be
         * addressed in the Lua file. User might choose 'ignore' to indicate he doesn't need it.
         * - Or choosing the ones you need. For instance for the hyperelasticity model present in the library, for
         * which the hyperelastic law is Cieralet Geymonat, solid parameters are:
         * \code
         InputParameter::Solid::VolumicMass,
         InputParameter::Solid::HyperelasticBulk,
         InputParameter::Solid::Kappa1,
         InputParameter::Solid::Kappa2,
         * \endcode
         */
        struct Solid : public Crtp::Section<Solid, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = Solid;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! Convenient alias.
            using VolumicMass = MaterialProperty::VolumicMass<self>;


            //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
            struct PoissonRatio : public Crtp::Section<PoissonRatio, Solid>
            {

                //! Convenient alias.
                using self = PoissonRatio;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines Poisson coefficient value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;


            }; // struct PoissonRatio


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct HyperelasticBulk : public Crtp::Section<HyperelasticBulk, Solid>
            {


                //! Convenient alias.
                using self = HyperelasticBulk;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct HyperelasticBulk


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Kappa1 : public Crtp::Section<Kappa1, Solid>
            {


                //! Convenient alias.
                using self = Kappa1;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the kappa 1 (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines kappa 1 value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct Kappa1



            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Kappa2 : public Crtp::Section<Kappa2, Solid>
            {

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                //! Convenient alias.
                using self = Kappa2;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



                /*!
                 * \brief Choose how is described the kappa 2 (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines kappa 2 value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };



                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };



                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;


            }; // struct Kappa2


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct YoungModulus : public Crtp::Section<YoungModulus, Solid>
            {

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                //! Convenient alias.
                using self = YoungModulus;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                /*!
                 * \brief Choose how is described the Young modulus (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines Young modulus value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                {

                };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;




            }; // struct YoungModulus


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct LameLambda : public Crtp::Section<LameLambda, Solid>
            {


                //! Convenient alias.
                using self = LameLambda;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct LameLambda


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct LameMu : public Crtp::Section<LameMu, Solid>
            {


                //! Convenient alias.
                using self = LameMu;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct LameMu


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Viscosity : public Crtp::Section<Viscosity, Solid>
            {


                //! Convenient alias.
                using self = Viscosity;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct Viscosity


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Mu1 : public Crtp::Section<Mu1, Solid>
            {


                //! Convenient alias.
                using self = Mu1;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct Mu1

            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Mu2 : public Crtp::Section<Mu2, Solid>
            {


                //! Convenient alias.
                using self = Mu2;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct Mu2


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct C0 : public Crtp::Section<C0, Solid>
            {


                //! Convenient alias.
                using self = C0;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct C0


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct C1 : public Crtp::Section<C1, Solid>
            {


                //! Convenient alias.
                using self = C1;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct C1


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct C2 : public Crtp::Section<C2, Solid>
            {


                //! Convenient alias.
                using self = C2;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct C2


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct C3 : public Crtp::Section<C3, Solid>
            {


                //! Convenient alias.
                using self = C3;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Solid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the hyperelastic bulk (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                public Impl::Nature
                { };


                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines hyperelastic bulk value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;



            }; // struct C3


            //! Holds information related to the input parameter Solid::PlaneStressStrain.
            struct PlaneStressStrain : public Crtp::InputParameter<PlaneStressStrain, self, std::string>
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                VolumicMass,
                PlaneStressStrain,
                HyperelasticBulk,
                Kappa1,
                Kappa2,
                PoissonRatio,
                YoungModulus,
                LameLambda,
                LameMu,
                Viscosity,
                Mu1,
                Mu2,
                C0,
                C1,
                C2,
                C3
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct Solid


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOLID_x_SOLID_HPP_
