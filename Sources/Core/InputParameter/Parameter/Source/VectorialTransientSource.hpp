///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/Source/Impl/TransientSource.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct VectorialTransientSource
        : public Crtp::Section<VectorialTransientSource<IndexT>, NoEnclosingSection>
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'TransientSource1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = VectorialTransientSource<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
             */
            struct Nature
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Nature, self, TransientSourceNS::VectorialNature::storage_type>,
            public TransientSourceNS::VectorialNature
            { };

            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Scalar, self, std::vector<double>>,
            public TransientSourceNS::Scalar
            { };


            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionX
            : public Crtp::InputParameter<LuaFunctionX, self, TransientSourceNS::LuaFunctionX::storage_type>,
            public TransientSourceNS::LuaFunctionX
            { };


            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionY
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<LuaFunctionY, self, TransientSourceNS::LuaFunctionY::storage_type>,
            public TransientSourceNS::LuaFunctionY
            { };



            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunctionZ
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<LuaFunctionZ, self, TransientSourceNS::LuaFunctionZ::storage_type>,
            public TransientSourceNS::LuaFunctionZ
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Nature,
                Scalar,
                LuaFunctionX,
                LuaFunctionY,
                LuaFunctionZ
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;

        }; // struct TransientSource


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/Source/VectorialTransientSource.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_
