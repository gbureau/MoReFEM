///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:08:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_

# include <string>


namespace MoReFEM
{


    //! Whether \a Coords or \a LocalCoords are considered.
    enum class CoordsType
    {
        local,
        global
    };


    namespace InputParameter
    {

        /*!
         * \brief Input parameter which wraps a Lua function with three arguments for spatial coordinates.
         *
         * \tparam CoordsTypeT Whether we deal with local or global coordinates for the function.
         */
        template<CoordsType CoordsTypeT>
        struct SpatialFunction
        {

            //! Returns the type of Coords.
            static CoordsType GetCoordsType();

            //! Part of the description related to the format of the spatial function.
            static const std::string& DescriptionCoordsType();


        };



    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/SpatialFunction.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_
