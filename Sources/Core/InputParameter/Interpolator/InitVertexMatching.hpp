///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HPP_

# include <memory>
# include <vector>


# include <string>

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Interpolator/Impl/InitVertexMatching.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {

            /*!
             * \brief Common base class from which all InputParameter::InitVertexMatchingInterpolator should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct InitVertexMatchingInterpolator
            { };



        } // namespace BaseNS




        /*!
         * \brief Holds information related to the input parameter InitVertexMatchingInterpolator.
         *
         * \tparam IndexT An interpolator requires at least two such objects (one for source and another for target).
         * Index is used to tag each of them.
         *
         * \code
         * InitVertexMatchingInterpolator1 = { # 1 is the index here
         * felt_space = 1,
         * numbering_subset = 3
         * ...
         * }
         *
         * InitVertexMatchingInterpolator2 = { # 2 is the index here
         * felt_space = 2,
         * numbering_subset = 1
         * ...
         * }
         *
         * \endcode
         */
        template<unsigned int IndexT>
        struct InitVertexMatchingInterpolator
        : public Crtp::Section<InitVertexMatchingInterpolator<IndexT>, NoEnclosingSection>,
        public BaseNS::InitVertexMatchingInterpolator
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'InitVertexMatchingInterpolator1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = InitVertexMatchingInterpolator<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Finite element space.
             */
            struct FEltSpaceIndex : public Crtp::InputParameter<FEltSpaceIndex, self, unsigned int>,
            public Impl::InitVertexMatchingInterpolatorNS::FEltSpaceIndexImpl
            { };


            /*!
             * \brief Format of the mesh file.
             */
            struct NumberingSubsetIndex : public Crtp::InputParameter<NumberingSubsetIndex, self, unsigned int>,
            public Impl::InitVertexMatchingInterpolatorNS::NumberingSubsetIndexImpl
            { };



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                FEltSpaceIndex,
                NumberingSubsetIndex
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct Mesh


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Interpolator/InitVertexMatching.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HPP_
