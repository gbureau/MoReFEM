///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
# define MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

# include "Core/InputParameter/Solver/Petsc.hpp"
# include "Core/InputParameterData/InputParameterList.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Init Petsc solver.
     *
     *
     * \copydetails doxygen_hide_mpi_param
     * \copydoc doxygen_hide_input_parameter_data_arg
     * \param[in] snes_function If linear solve, just give nullptr. If non linear solve, give here the function
     * evaluation routine. See Petsc documentation
     * http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESSetFunction.html for more details.
     * \param[in] snes_jacobian If non linear solve, give here the function that computes the jacobian. See Petsc
     * documentation
     * http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESSetJacobian.html for more details.
     * \param[in] snes_viewer If non linear solve, give here the function that may print some informations at each
     * iteration step. See Petsc documentation
     * http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESMonitorSet.html for more details.
     * \param[in] snes_convergence_test_function If non linear solve, give here the functional form used for testing of
     *  convergence of nonlinear solver. See Petsc documentation
     * http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESConvergenceTestFunction.html for more
     * details.
     *
     * \return \a Wrappers::Petsc::Snes correctly initialized.
     */
    template<unsigned int SolverIndexT, class InputParameterDataT>
    Wrappers::Petsc::Snes::unique_ptr InitSolver(const Wrappers::Mpi& mpi,
                                                 const InputParameterDataT& input_parameter_data,
                                                 Wrappers::Petsc::Snes::SNESFunction snes_function = nullptr,
                                                 Wrappers::Petsc::Snes::SNESJacobian snes_jacobian = nullptr,
                                                 Wrappers::Petsc::Snes::SNESViewer snes_viewer = nullptr,
                                                 Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function = nullptr);



    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/Solver/Solver.hxx"


#endif // MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
