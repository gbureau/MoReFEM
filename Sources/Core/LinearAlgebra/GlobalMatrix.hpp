///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 27 Apr 2015 09:33:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_MATRIX_HPP_
# define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_MATRIX_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

# include "Core/Crtp/NumberingSubsetForMatrix.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Class which encapsulates both the Petsc matrix and the numbering subsets used to described its
     * rows and columns.
     */
    class GlobalMatrix : public Wrappers::Petsc::Matrix,
                         public Crtp::NumberingSubsetForMatrix<GlobalMatrix>
    {

    public:

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GlobalMatrix>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to an array of unique pointers.
        template<std::size_t I>
        using array_unique_ptr = std::array<unique_ptr, I>;

        //! Alias to parent.
        using petsc_parent = Wrappers::Petsc::Matrix;

        //! Alias to other parent.
        using numbering_subset_parent = Crtp::NumberingSubsetForMatrix<GlobalMatrix>;


    public:

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit GlobalMatrix(const NumberingSubset& row_numbering_subset,
                              const NumberingSubset& col_numbering_subset);

        //! Destructor.
        ~GlobalMatrix() override;

        //! Copy constructor.
        GlobalMatrix(const GlobalMatrix&);

        //! Move constructor.
        GlobalMatrix(GlobalMatrix&&) = delete;

        //! Copy affectation.
        GlobalMatrix& operator=(const GlobalMatrix&) = delete;

        //! Move affectation.
        GlobalMatrix& operator=(GlobalMatrix&&) = delete;

        ///@}

    private:

        // ===========================================================================
        // \attention Do not forget to update Swap() if a new data member is added!
        // =============================================================================


    };


    # ifndef NDEBUG

    /*!
     * \class doxygen_hide_assert_matrix_respect_pattern_function
     *
     * \brief Check whether a matrix really follows the pattern expected from its numbering subset.
     *
     * If not fulfilled, the program is aborted.
     *
     * \param[in] matrix Matrix being investigated.
     * \copydetails doxygen_hide_invoking_file_and_line
     */



    /*!
     * \brief Assert two matrices share the same \a NumberingSubset.
     *
     * \param[in] matrix1 First matrix.
     * \param[in] matrix2 Second matrix.
     */
    void AssertSameNumberingSubset(const GlobalMatrix& matrix1,
                                   const GlobalMatrix& matrix2);


    /*!
     * \brief Debug tool to print the unique ids of row and column \a NumberingSubset.
     *
     * \param[in] matrix_name Tag to identify the matrix which \a NumberingSubset informations will be written.
     * \param[in] matrix Matrix under investigation.
     */
    void PrintNumberingSubset(std::string&& matrix_name,
                              const GlobalMatrix& matrix);



    # endif // NDEBUG


    /*!
     * \brief Swap two matrices.
     *
     * The Petsc content of the matrices is swapped; however the numbering subsets must be the same on both ends
     * (we expect here to swap only matrices with same structure).
     *
     * \attention Do not use it until #530 is resolved; Petsc's defined swap might have to be used.
     *
     * \param[in] A One of the matrix to swap.
     * \param[in] B The other matrix.
     */
    void Swap(GlobalMatrix& A, GlobalMatrix& B);


    /*!
     * \class doxygen_hide_global_linear_algebra_forbidden_swap_function
     *
     * \brief Declared but do not defined: we do not want to be able to do this but we also want to avoid the
     * 'slicing effect' (i.e. attributes of child class ignored entirely).
     */

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrix& , GlobalMatrix::petsc_parent& );

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrix::petsc_parent& , GlobalMatrix& );




    /*!
     * \brief Useful alias to avoid cluttering the main programs with too low-level C++.
     *
     * \code
     * GlobalMatrixWithCoefficient(global_matrix, 1.)
     * \endcode
     *
     * is probably easier to grasp than either:
     * \code
     * std::pair<GlobalMatrix&, double>(global_matrix, 1.)
     * \endcode
     *
     * or
     *
     * \code
     * std::make_pair(std::ref(global_matrix, 1.)
     * \endcode
     */
    using GlobalMatrixWithCoefficient = std::pair<GlobalMatrix&, double>;


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/LinearAlgebra/GlobalMatrix.hxx"


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_MATRIX_HPP_
