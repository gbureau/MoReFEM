///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Aug 2013 10:44:10 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HXX_


namespace MoReFEM
{



    template<class TupleT>
    InputParameterList<TupleT>::InputParameterList(const std::string& filename,
                                                   const Wrappers::Mpi& mpi,
                                                   Utilities::InputParameterListNS::DoTrackUnusedFields do_track_unused_fields)
    : Utilities::InputParameterListNS::Base<InputParameterList, TupleT>(filename,
                                                                        mpi,
                                                                        do_track_unused_fields)
    {
        // \todo Deactivated for the moment, see #167
        // CheckVariableConsistency();
        // CheckBoundaryConditionConsistency();
    }


//    template<class TupleT>
//    void InputParameterList<TupleT>::CheckVariableConsistency() const
//    {
//        using SubTuple = std::tuple
//        <
//            InputParameter::Variable::Variable,
//            InputParameter::Variable::DegreeOfExactness
//        >;
//
//        this->template EnsureSameLength<SubTuple>();
//    }


//    template<class TupleT>
//    void InputParameterList<TupleT>::CheckBoundaryConditionConsistency() const
//    {
//        using SubTuple = std::tuple
//        <
//            InputParameter::BoundaryCondition::Variable,
//            InputParameter::BoundaryCondition::Component
//        >;
//
//        this->template EnsureSameLength<SubTuple>();
//    }



} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_INPUT_PARAMETER_LIST_HXX_
