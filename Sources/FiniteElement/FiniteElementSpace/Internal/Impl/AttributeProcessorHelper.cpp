///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 May 2014 15:59:32 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Impl/AttributeProcessorHelper.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            namespace Impl
            {
             

                          
                AttributeProcessorHelper::AttributeProcessorHelper(const unsigned int Nprocessor)
                {
                    Nlocal_felt_space_per_processor_.resize(Nprocessor, 0);
                    processor_for_each_geom_elt_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                }
                
                
                unsigned int AttributeProcessorHelper
                ::ProcessorForCurrentLocalFEltSpace(const LocalFEltSpace& local_felt_space)
                {
                    const auto local_felt_space_id = local_felt_space.GetGeometricElt().GetIndex();
                    
                    // First check whether a choice has already be made by a previous finite element space.
                    // If so, stick with it.
                    {
                        auto it = processor_for_each_geom_elt_.find(local_felt_space_id);
                    
                        if (it != processor_for_each_geom_elt_.cend())
                            return it->second;
                    }
                    
                    const auto& node_bearer_list = local_felt_space.GetNodeBearerList();
                    
                    // If there is a node bearer on volume, assign the finite element group to the same processor.
                    {
                        auto begin = node_bearer_list.cbegin();
                        auto end = node_bearer_list.cend();
                        
                        auto is_node_on_volume = [](const NodeBearer::shared_ptr& node_bearer_ptr)
                        {
                            assert(!(!node_bearer_ptr));
                            return node_bearer_ptr->GetNature() == ::MoReFEM::InterfaceNS::Nature::volume;
                        };
                        
                        auto it = std::find_if(begin, end, is_node_on_volume);
                        
                        
                        if (it != end)
                        {
                            assert(std::count_if(begin, end, is_node_on_volume) == 1u
                                   && "There should be at most one node bearer on volume, and a test has already be passed "
                                   "to ensure there was at least one...");
                            
                            auto node_bearer_on_volume_ptr = *it;
                            const unsigned int ret = node_bearer_on_volume_ptr->GetProcessor();
                            
                            // Now update the number of finite element groups per processor accordingly.
                            assert(ret < Nlocal_felt_space_per_processor_.size());
                            ++Nlocal_felt_space_per_processor_[ret];

                            auto check = processor_for_each_geom_elt_.insert({local_felt_space_id, ret});
                            assert(check.second);
                            static_cast<void>(check);
                            
                            return ret;
                        }
                        
                    }
                    
                    // If not, look the processor that holds the most node bearers.
                    auto&& Nnode_bearer_on_each_processor = NnodeBearerPerProcessor(node_bearer_list);
                    
                    assert(Nnode_bearer_on_each_processor.size() == Nlocal_felt_space_per_processor_.size());
                    
                    auto max_position = std::max_element(Nnode_bearer_on_each_processor.cbegin(),
                                                         Nnode_bearer_on_each_processor.cend());
                    const unsigned int max_value = *max_position;
                    
                    const unsigned int size = static_cast<unsigned int>(Nnode_bearer_on_each_processor.size());
                    
                    unsigned int ret = NumericNS::UninitializedIndex<unsigned int>();
                    
                    // Is the maximum unique or not?
                    if (std::count(Nnode_bearer_on_each_processor.cbegin(), Nnode_bearer_on_each_processor.cend(), max_value) == 1)
                        ret = static_cast<unsigned int>(max_position - Nnode_bearer_on_each_processor.cbegin());
                    // If not, attribute the geometric element to the one with the most ghosts already.
                    else
                    {
                        std::vector<unsigned int> candidates;
                        
                        for (unsigned int i = 0; i < size; ++i)
                        {
                            if (Nnode_bearer_on_each_processor[i] == max_value)
                                candidates.push_back(i);
                        }
                        
                        // Choose among candidates the one with most ghosts.
                        assert(candidates.size() >= 2);
                        
                        unsigned int Nfelt_min = std::numeric_limits<unsigned int>::max();
                        
                        for (unsigned int candidate : candidates)
                        {
                            assert(candidate < Nlocal_felt_space_per_processor_.size());
                            
                            if (NfiniteElt(candidate) < Nfelt_min)
                            {
                                ret = candidate;
                                Nfelt_min = NfiniteElt(candidate);
                            }
                        }
                    } // else
                    
                    // Now update the number of local finite element spaces per processor accordingly.
                    assert(ret < Nlocal_felt_space_per_processor_.size());
                    ++Nlocal_felt_space_per_processor_[ret];
                    
                    auto check = processor_for_each_geom_elt_.insert({local_felt_space_id, ret});
                    assert(check.second);
                    static_cast<void>(check);
                    
                    return ret;
                }
                
                
                unsigned int AttributeProcessorHelper::NfiniteElt(unsigned int processor) const
                {
                    assert(processor < Nlocal_felt_space_per_processor_.size());
                    return Nlocal_felt_space_per_processor_[processor];
                }
                
                
                unsigned int AttributeProcessorHelper::Nprocessor() const
                {
                    return static_cast<unsigned int>(Nlocal_felt_space_per_processor_.size());
                }
                
                
                std::vector<unsigned int> AttributeProcessorHelper
                ::NnodeBearerPerProcessor(const NodeBearer::vector_shared_ptr& node_bearer_list) const
                {
                    const unsigned int Nprocessor = this->Nprocessor();
                    std::vector<unsigned int> ret(Nprocessor, 0);
                    
                    for (const auto& node_bearer_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        auto node_bearer_processor = node_bearer_ptr->GetProcessor();
                        
                        assert(node_bearer_processor < Nprocessor);
                        
                        ++ret[node_bearer_processor];
                    }

                    return ret;
                }


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
