///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"
# include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle3.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"



namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TriangleP1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Triangle class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TriangleP1
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Triangle,
            RefGeomEltNS::ShapeFunctionNS::Triangle3,
            InterfaceNS::Nature::vertex,
            0u
        >
        {

        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TriangleP1() = default;

            //! Destructor.
            ~TriangleP1() override;

            //! Copy constructor.
            TriangleP1(const TriangleP1&) = default;

            //! Move constructor.
            TriangleP1(TriangleP1&&) = default;

            //! Affectation.
            TriangleP1& operator=(const TriangleP1&) = default;

            //! Affectation.
            TriangleP1& operator=(TriangleP1&&) = default;

            ///@}

        };



    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_HPP_
