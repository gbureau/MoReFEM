///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 10:38:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HPP_


# include <vector>
# include <memory>
# include <cassert>
# include <map>

# include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"

# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Defines a quadrature rule.
     */
    class QuadratureRule
    {

    public:

        //! Alias to shared_ptr to constant object.
        using const_shared_ptr = std::shared_ptr<const QuadratureRule>;

        //! Alias to unique_ptr to constant object.
        using const_unique_ptr = std::unique_ptr<const QuadratureRule>;

        //! Alias to a vector of const_shared_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;


    public:

        /*!
         * \class doxygen_hide_quadrature_rule_constructor_args
         *
         * \param[in] topology_id Topology identifier.
         * \param[in] degree_of_exactness Degree of exactness of the rule. Choose NumericNS::UninitiazedIndex() for
         * rules for which it is pointless.
         *
         * \todo This should be handled more properly with inheritance, so that rules for which it is pointless do not
         * even have such an attribute, but convention above will do for the time being.
         */

        /*!
         * \class doxygen_hide_quadrature_rule_constructor_name_arg
         *
         * \param[in] name Name of the quadrature rule.
         */


        /// \name Constructors and destructor.
        ///@{

        /*!
         * Constructor.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         * \copydetails doxygen_hide_quadrature_rule_constructor_name_arg
         * \param[in] point_list List of quadrature points.
         */
        explicit QuadratureRule(std::string&& name,
                                QuadraturePoint::vector_const_shared_ptr&& point_list,
                                RefGeomEltNS::TopologyNS::Type topology_id,
                                unsigned int degree_of_exactness);

        /*!
         * Constructor.
         *
         * In this overload, quadrature points aren't yet defined and must be added afterwards through
         * \a AddQuadraturePoint() method.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         * \copydetails doxygen_hide_quadrature_rule_constructor_name_arg
         */
        explicit QuadratureRule(std::string&& name,
                                RefGeomEltNS::TopologyNS::Type topology_id,
                                unsigned int degree_of_exactness = NumericNS::UninitializedIndex<unsigned int>());

        /*!
         * \brief Dummy constructor with only degree of exactness defined.
         *
         * Should be used solely to ease definition of sorting functions.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         *
         * \code
         *
         * auto sorting_rule = [](const GetQuadratureRule& rule1, const GetQuadratureRule& rule2)
         * {
         *      return rule1.DegreeOfExactness() < rule2.DegreeOfExactness();
         * };
         *
         * std::vector<QuadratureRule> list;
         * ... defines your list ...
         *
         * std::lower_bound(list.cbegin(), list.cend(), GetQuadratureRule(5), sorting_rule);
         *
         * \endcode
         */
        explicit QuadratureRule(RefGeomEltNS::TopologyNS::Type topology_id,
                                unsigned int degree_of_exactness);

        //! Destructor.
        ~QuadratureRule() = default;

        //! Copy constructor.
        QuadratureRule(const QuadratureRule&) = delete;

        //! Move constructor.
        QuadratureRule(QuadratureRule&&) = default;

        //! Copy affectation.
        QuadratureRule& operator=(const QuadratureRule&) = delete;

        //! Move affectation.
        QuadratureRule& operator=(QuadratureRule&&) = delete;


        ///@}


        /*!
         * \brief Add a new quadrature point.
         *
         * The quadrature point is created inside the function from \a local_coords and \a weight.
         *
         * \param[in] local_coords Local coordinates of the quadrature point to create.
         * \param[in] weight Weight of the quadrature point to create.
         */
        void AddQuadraturePoint(LocalCoords&& local_coords, double weight);

        //! Number of quadrature points.
        unsigned int NquadraturePoint() const noexcept;

        //! Access to one quadrature point.
        const QuadraturePoint& Point(unsigned int index) const noexcept;

        //! Return the degree of exactness if relevant (and will assert in debug if not).
        unsigned int DegreeOfExactness() const noexcept;

        //! Identifier of the topology upon which the rule is defined.
        RefGeomEltNS::TopologyNS::Type GetTopologyIdentifier() const noexcept;

        //! Returns the name of the quadrature rule.
        const std::string& GetName() const noexcept;

        //! Returns the list of quadrature points.
        const QuadraturePoint::vector_const_shared_ptr& GetQuadraturePointList() const noexcept;


    private:

        //! Name of the quadrature rule.
        std::string name_;

        //! List of quadrature points.
        QuadraturePoint::vector_const_shared_ptr point_list_;

        //! Identifier of the geometric element upon which the rule is defined.
        const RefGeomEltNS::TopologyNS::Type topology_id_;

        /*!
         * \brief Degree of exactness.
         *
         * Equal to NumericNS::UninitiazedIndex() for rules for which it is pointless.
         *
         * \todo This should be handled more properly with inheritance, so that rules for which it is pointless do not
         * even have such an attribute, but convention above will do for the time being.
         */
        const unsigned int degree_of_exactness_;
    };



    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/QuadratureRules/QuadratureRule.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HPP_
