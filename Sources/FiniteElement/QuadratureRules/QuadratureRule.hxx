///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 10:38:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HXX_


namespace MoReFEM
{


    inline unsigned int QuadratureRule::NquadraturePoint() const noexcept
    {
        assert(!point_list_.empty());
        return static_cast<unsigned int>(point_list_.size());
    }


    inline const QuadraturePoint& QuadratureRule::Point(unsigned int index) const noexcept
    {
        assert(!point_list_.empty());
        assert(index < point_list_.size());
        assert(!(!point_list_[index]));
        return *(point_list_[index]);
    }


    inline unsigned int QuadratureRule::DegreeOfExactness() const noexcept
    {
        assert(degree_of_exactness_ != NumericNS::UninitializedIndex<unsigned int>()
               && "Should be called only if it is relevant for the law!");
        return degree_of_exactness_;
    }


    inline RefGeomEltNS::TopologyNS::Type QuadratureRule::GetTopologyIdentifier() const noexcept
    {
        return topology_id_;
    }


    inline const std::string& QuadratureRule::GetName() const noexcept
    {
        assert(!name_.empty());
        return name_;
    }


    inline const QuadraturePoint::vector_const_shared_ptr& QuadratureRule::GetQuadraturePointList() const noexcept
    {
        assert(!point_list_.empty());
        return point_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_HXX_
