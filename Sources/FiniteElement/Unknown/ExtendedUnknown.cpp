///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Apr 2015 10:40:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{
    
    
    ExtendedUnknown::ExtendedUnknown(Unknown::const_shared_ptr unknown,
                                     NumberingSubset::const_shared_ptr numbering_subset,
                                     const std::string& shape_function_label)
    : unknown_(unknown),
    numbering_subset_(numbering_subset),
    shape_function_label_(shape_function_label)
    { }

    

} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
