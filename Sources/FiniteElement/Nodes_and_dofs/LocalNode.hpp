///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HPP_

# include <memory>
# include <vector>
# include <iosfwd>

# include "Utilities/Numeric/Numeric.hpp"

# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/Coords/LocalCoords.hpp"
# include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp"


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief A LocalNode is the pendant of a geometric interface in a \a BasicRefFElt.
     */
    class LocalNode final
    {

    public:

        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const LocalNode>;

        //! Alias to vector of shared_pointer.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] local_interface Local interface upon which the LocalNode is built.
         * \param[in] index Incremental index of the local nodes in the BasicRefFElt. All local nodes on vertices
         * are numbered first; then all edges, and so on.
         * \param[in] local_coords Approximate position of the local node.
         */
        LocalNode(RefGeomEltNS::TopologyNS::LocalInterface&& local_interface,
                  unsigned int index,
                  const LocalCoords& local_coords);

        //! Destructor.
        ~LocalNode() = default;

        //! Copy constructor.
        LocalNode(const LocalNode&) = delete;

        //! Move constructor.
        LocalNode(LocalNode&&) = delete;

        //! Affectation.
        LocalNode& operator=(const LocalNode&) = delete;

        //! Move affectation.
        LocalNode& operator=(LocalNode&&) = delete;

        ///@}


        //! Get the local interface.
        const RefGeomEltNS::TopologyNS::LocalInterface& GetLocalInterface() const noexcept;

        //! Get the local index of the dof.
        unsigned int GetIndex() const noexcept;

        //! Get the local coordinates of the dof.
        const LocalCoords& GetLocalCoords() const noexcept;

        //! Print the informations of the local dof.
        void Print(std::ostream& out) const;


    private:

        //! Local interface.
        const RefGeomEltNS::TopologyNS::LocalInterface local_interface_;

        //! Local index.
        const unsigned int index_;

        //! Local coordinates.
        LocalCoords local_coords_;
    };


    //! Operator<; the criterion used is the index.
    bool operator<(const LocalNode& lhs, const LocalNode& rhs);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/Nodes_and_dofs/LocalNode.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HPP_
