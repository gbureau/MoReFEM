///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Feb 2016 14:06:43 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            template
            <
                class LuaFieldT,
                class InputParameterDataT
            >
            InitialCondition<ParameterNS::Type::scalar>::unique_ptr
            InitScalarInitialCondition(const Mesh& mesh,
                                       const InputParameterDataT& input_parameter,
                                       const std::string& nature,
                                       const double scalar_value)
            {
                namespace IPL = Utilities::InputParameterListNS;

                if (nature == "constant")
                {
                    using initial_condition_type = Impl::InitialConditionInstance
                    <
                        ParameterNS::Type::scalar,
                        Policy::Constant
                    >;

                    return std::make_unique<initial_condition_type>(mesh, scalar_value);
                }
                else if (nature == "lua_function")
                {
                    using initial_condition_type = Impl::InitialConditionInstance
                    <
                        ParameterNS::Type::scalar,
                        Policy::LuaFunction,
                        LuaFieldT
                    >;

                    decltype(auto) value =
                        IPL::Extract<LuaFieldT>::Value(input_parameter);

                    return std::make_unique<initial_condition_type>(mesh, value);
                }
                else
                {
                    assert(false && "Should not happen: all the possible choices are assumed to be checked by "
                           " LuaOptionFile Constraints.");
                }

                return nullptr;
            }


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_
