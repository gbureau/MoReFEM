///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HPP_

# include <memory>
# include <vector>


# include "Utilities/InputParameterList/LuaFunction.hpp"
# include "Utilities/InputParameterList/LuaFunction.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"

# include "Geometry/Coords/Coords.hpp"

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Policy
            {


                /*!
                 * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
                 * input parameter file.
                 *
                 * \tparam TypeT  Type of the parameter (real, vector, matrix).
                 */
                template<ParameterNS::Type TypeT, class SpatialFunction>
                class LuaFunction
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = LuaFunction<TypeT, SpatialFunction>;

                private:

                    //! Alias to traits class related to TypeT.
                    using traits = ::MoReFEM::ParameterNS::Traits<TypeT>;

                public:

                    //! Alias to return type.
                    using return_type = typename traits::return_type;

                    //! Alias to the storage of the Lua function.
                    using storage_type = const typename SpatialFunction::return_type&;


                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit LuaFunction(const Mesh& mesh,
                                         storage_type lua_function);

                    //! Destructor.
                    ~LuaFunction() = default;

                    //! Copy constructor.
                    LuaFunction(const LuaFunction&) = delete;

                    //! Move constructor.
                    LuaFunction(LuaFunction&&) = delete;

                    //! Copy affectation.
                    LuaFunction& operator=(const LuaFunction&) = delete;

                    //! Move affectation.
                    LuaFunction& operator=(LuaFunction&&) = delete;

                    ///@}

                protected:

                    /*!
                     * \brief Provided here to make the code compile, but should never be called
                     *
                     * (Constant value should not be given through a function: it is less efficient and the constness
                     * is not appearant in the code...).
                     *
                     * \return Spatially constant value.
                     */
                    [[noreturn]] return_type GetConstantValueFromPolicy() const;

                    //! Return the value for \a coords.
                    return_type GetValueFromPolicy(const SpatialPoint& coords) const;


                protected:

                    /*!
                     * \brief Whether the parameter varies spatially or not.
                     *
                     * \return False: for LuaFunction it is always assumed to be false. If it is indeed constant,
                     * rather use the dedicated \a Constant policy.
                     */
                    bool IsConstant() const noexcept;


                private:

                    //! Store a reference to the Lua function (which is owned by the InputParameterList class).
                    storage_type lua_function_;


                };


            } // namespace Policy


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HPP_
