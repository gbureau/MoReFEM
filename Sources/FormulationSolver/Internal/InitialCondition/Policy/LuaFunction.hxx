///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Policy
            {


                template<ParameterNS::Type TypeT, class SpatialFunctionT>
                LuaFunction<TypeT, SpatialFunctionT>::LuaFunction(const Mesh& ,
                                                                  storage_type lua_function)
                : lua_function_(lua_function)
                { }


                template<ParameterNS::Type TypeT, class SpatialFunctionT>
                typename LuaFunction<TypeT, SpatialFunctionT>::return_type
                LuaFunction<TypeT, SpatialFunctionT>::GetValueFromPolicy(const SpatialPoint& coords) const
                {
                    return lua_function_(coords.x(), coords.y(), coords.z());
                }


                template<ParameterNS::Type TypeT, class SpatialFunctionT>
                [[noreturn]] typename LuaFunction<TypeT, SpatialFunctionT>::return_type
                LuaFunction<TypeT, SpatialFunctionT>::GetConstantValueFromPolicy() const
                {
                    assert(false && "A Lua function should yield IsConstant() = false!");
                    exit(-1);
                }


                template<ParameterNS::Type TypeT, class SpatialFunctionT>
                bool LuaFunction<TypeT, SpatialFunctionT>::IsConstant() const noexcept
                {
                    return false;
                }


            } // namespace Policy


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_
