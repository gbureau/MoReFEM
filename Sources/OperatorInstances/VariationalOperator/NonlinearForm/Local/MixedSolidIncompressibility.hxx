///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_MIXED_SOLID_INCOMPRESSIBILITY_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_MIXED_SOLID_INCOMPRESSIBILITY_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {

        template<class HydrostaticLawPolicyT>
        MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::MixedSolidIncompressibility(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                      const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                      elementary_data_type&& a_elementary_data,
                                      const cauchy_green_tensor_type& cauchy_green_tensor,
                                      const HydrostaticLawPolicyT* hydrostatic_law)
        : NonlinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent(),
        cauchy_green_tensor_(cauchy_green_tensor),
        hydrostatic_law_(hydrostatic_law)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_displacement = static_cast<int>(displacement_ref_felt.Nnode());

            const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            const int Nnode_pressure = static_cast<int>(pressure_ref_felt.Nnode());
            
            const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
            const int Nnode_test_displacement = static_cast<int>(test_displacement_ref_felt.Nnode());
            
            const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));
            const int Nnode_test_pressure = static_cast<int>(test_pressure_ref_felt.Nnode());

            const unsigned int Ncomponent = elementary_data.GetGeomEltDimension();
            const unsigned int square_Ncomponent = NumericNS::Square(Ncomponent);

            unsigned int engeneering_vect_size(0u);

            switch(Ncomponent)
            {
                case 2:
                    engeneering_vect_size = 3u;
                    break;
                case 3:
                    engeneering_vect_size = 6u;
                    break;
                default:
                    assert(false);
                    break;
            }

            former_local_displacement_.resize(elementary_data.GetRefFElt(GetNthUnknown(0)).Ndof());
            
            former_local_pressure_.resize(elementary_data.GetRefFElt(GetNthUnknown(1)).Ndof());

            this->matrix_parent::InitLocalMatrixStorage
            ({{
                { square_Ncomponent, square_Ncomponent },           // tangent_matrix_disp_disp
                { square_Ncomponent, 1 },                           // tangent_matrix_disp_pres
                { 1, square_Ncomponent },                           // tangent_matrix_pres_disp
                { Ncomponent, Nnode_displacement },                 // transposed_dphi_displacement
                { Ncomponent, Ncomponent},                          // displacement_gradient,
                { engeneering_vect_size, engeneering_vect_size },   // hydrostatic_tangent
                { Ncomponent, Ncomponent },                         // gradient-based block
                { Nnode_test_displacement, Nnode_displacement },    // block_contribution
                { Nnode_test_displacement, Ncomponent },            // dphi_test_disp_mult_gradient_based_block
                { square_Ncomponent, square_Ncomponent },           // linear_part
                { square_Ncomponent, engeneering_vect_size },       // linear_part_intermediate_matrix
                { Ncomponent, 1 },                                  // column_matrix,
                { Nnode_test_displacement, 1 },                     // dphi_test_disp_mult_column_matrix,
                { Nnode_test_displacement, Nnode_pressure },        // block_contribution_disp_pres,
                { 1, Ncomponent },                                  // row_matrix,
                { 1, Nnode_displacement },                          // row_matrix_mult_transposed_dphi_disp,
                { Nnode_test_pressure, Nnode_displacement }         // block_contribution_pres_disp
            }});


            this->vector_parent::InitLocalVectorStorage
            ({{
                square_Ncomponent,          // rhs_disp
                engeneering_vect_size,      // hysdrostatic_stress
                engeneering_vect_size,      // diff_hydrostatic_stress_wrt_pres
                square_Ncomponent,          // tangent_vector_disp_pres
                square_Ncomponent           // tangent_vector_pres_disp
            }});
            
            deriv_green_lagrange_ =
                std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(Ncomponent);
            
            invariant_holder_ = std::make_unique<invariant_holder_type>(Ncomponent,
                                                                        InvariantHolderNS::Content::invariants_and_first_and_second_deriv);
        }


        template<class HydrostaticLawPolicyT>
        MixedSolidIncompressibility<HydrostaticLawPolicyT>::~MixedSolidIncompressibility() = default;


        template<class HydrostaticLawPolicyT>
        const std::string& MixedSolidIncompressibility<HydrostaticLawPolicyT>::ClassName()
        {
            static std::string name("MixedSolidIncompressibility");
            return name;
        }


        template<class HydrostaticLawPolicyT>
        inline const std::vector<double>& MixedSolidIncompressibility<HydrostaticLawPolicyT>::GetFormerLocalDisplacement() const noexcept
        {
            return former_local_displacement_;
        }


        template<class HydrostaticLawPolicyT>
        inline std::vector<double>& MixedSolidIncompressibility<HydrostaticLawPolicyT>::GetNonCstFormerLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
        }


        template<class HydrostaticLawPolicyT>
        inline const std::vector<double>& MixedSolidIncompressibility<HydrostaticLawPolicyT>::GetFormerLocalPressure() const noexcept
        {
            return former_local_pressure_;
        }


        template<class HydrostaticLawPolicyT>
        inline std::vector<double>& MixedSolidIncompressibility<HydrostaticLawPolicyT>::GetNonCstFormerLocalPressure() noexcept
        {
            return const_cast<std::vector<double>&>(GetFormerLocalPressure());
        }

        template<class HydrostaticLawPolicyT>
        inline const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
        MixedSolidIncompressibility<HydrostaticLawPolicyT>::GetCauchyGreenTensor() const noexcept
        {
            return cauchy_green_tensor_;
        }

        template<class HydrostaticLawPolicyT>
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>& MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::GetNonCstDerivativeGreenLagrange() noexcept
        {
            assert(!(!deriv_green_lagrange_));
            return *deriv_green_lagrange_;
        }


        template<class HydrostaticLawPolicyT>
        void MixedSolidIncompressibility<HydrostaticLawPolicyT>::ComputeEltArray()
        {
            const auto& local_displacement = GetFormerLocalDisplacement();
            const auto& local_pressure = GetFormerLocalPressure();

            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            auto& vector_result = elementary_data.GetNonCstVectorResult();
            matrix_result.Zero();
            vector_result.Zero();

            auto& tangent_matrix_disp_disp = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_disp_disp)>();
            
            auto& tangent_vector_disp_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tangent_vector_disp_pres)>();
            auto& tangent_vector_pres_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tangent_vector_pres_disp)>();

            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());

            const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            
            const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
            const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const auto felt_space_dimension = displacement_ref_felt.GetFEltSpaceDimension();
            
            unsigned int quad_pt_index = 0;

            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];
                
                tangent_matrix_disp_disp.Zero();
                tangent_vector_disp_pres.Zero();
                tangent_vector_pres_disp.Zero();
                
                PrepareInternalDataForQuadraturePoint(infos_at_quad_pt_for_unknown,
                                                      infos_at_quad_pt_for_test_unknown,
                                                      geom_elt,
                                                      displacement_ref_felt,
                                                      pressure_ref_felt,
                                                      test_displacement_ref_felt,
                                                      test_pressure_ref_felt,
                                                      local_displacement,
                                                      local_pressure,
                                                      felt_space_dimension);
                
                ++quad_pt_index;
            }

        }

        template<class HydrostaticLawPolicyT>
        void MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::PrepareInternalDataForQuadraturePoint(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                                                const InformationsAtQuadraturePoint& infos_at_quad_pt_for_test_unknown,
                                                const GeometricElt& geom_elt,
                                                const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                                const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                                const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                                const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                                const std::vector<double>& local_displacement,
                                                const std::vector<double>& local_pressure,
                                                const unsigned int felt_space_dimension)
        {
            switch(felt_space_dimension)
            {
                case 2:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<2>(infos_at_quad_pt_for_unknown,
                                                                               infos_at_quad_pt_for_test_unknown,
                                                                               geom_elt,
                                                                               displacement_ref_felt,
                                                                               pressure_ref_felt,
                                                                               test_displacement_ref_felt,
                                                                               test_pressure_ref_felt,
                                                                               local_displacement,
                                                                               local_pressure);
                    break;
                }
                case 3:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<3>(infos_at_quad_pt_for_unknown,
                                                                               infos_at_quad_pt_for_test_unknown,
                                                                               geom_elt,
                                                                               displacement_ref_felt,
                                                                               pressure_ref_felt,
                                                                               test_displacement_ref_felt,
                                                                               test_pressure_ref_felt,
                                                                               local_displacement,
                                                                               local_pressure);
                    break;
                }
                default:
                    assert(false);
            }
        }

        template<class HydrostaticLawPolicyT>
        template<unsigned int FeltSpaceDimensionT>
        void MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::PrepareInternalDataForQuadraturePointForDimension(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                                                            const InformationsAtQuadraturePoint& infos_at_quad_pt_for_test_unknown,
                                                            const GeometricElt& geom_elt,
                                                            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                                            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                                            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                                            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                                            const std::vector<double>& local_displacement,
                                                            const std::vector<double>& local_pressure)
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            auto& vector_result = elementary_data.GetNonCstVectorResult();
            
            const unsigned int Ncomponent = displacement_ref_felt.GetMeshDimension();
            const int int_Ncomponent = static_cast<int>(Ncomponent);
            
            const unsigned int Nnode_disp = displacement_ref_felt.Nnode();
            const unsigned int Nnode_pressure = pressure_ref_felt.Nnode();
            const int int_Nnode_disp = static_cast<int>(Nnode_disp);
            const int int_Nnode_pressure = static_cast<int>(Nnode_pressure);
            
            const unsigned int Nnode_test_disp = test_displacement_ref_felt.Nnode();
            const unsigned int Nnode_test_pressure = test_pressure_ref_felt.Nnode();
            const int int_Nnode_test_disp = static_cast<int>(Nnode_test_disp);
            const int int_Nnode_test_pressure = static_cast<int>(Nnode_test_pressure);
            
            const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
            
            const auto weight_meas = quad_pt.GetWeight() * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
            
            auto& tangent_matrix_disp_disp = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_disp_disp)>();
            
            auto& tangent_vector_disp_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tangent_vector_disp_pres)>();
            auto& tangent_vector_pres_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tangent_vector_pres_disp)>();
            
            auto& linear_part_intermediate_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part_intermediate_matrix)>();
            auto& linear_part = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();
            
            auto& displacement_gradient = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
            
            auto& hydrostatic_stress = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::hydrostatic_stress)>();
            auto& diff_hydrostatic_stress_wrt_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::diff_hydrostatic_stress_wrt_pres)>();
            auto& hydrostatic_tangent = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::hydrostatic_tangent)>();
            
            auto& rhs_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_disp)>();
            
            auto& gradient_based_block = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
            auto& dphi_test_disp_mult_gradient_based_block =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_disp_mult_gradient_based_block)>();
            auto& block_contribution =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
            
            auto& transposed_dphi_displacement = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_displacement)>();
            
            auto& column_matrix = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::column_matrix)>();
            auto& dphi_test_disp_mult_column_matrix = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_disp_mult_column_matrix)>();
            auto& block_contribution_disp_pres = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution_disp_pres)>();
            auto& row_matrix = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::row_matrix)>();
            auto& row_matrix_mult_transposed_dphi_disp = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::row_matrix_mult_transposed_dphi_disp)>();
            auto& block_contribution_pres_disp = this->matrix_parent::template
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution_pres_disp)>();
            
            const auto& grad_felt_phi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi(); // on (u p)
            const auto& test_grad_felt_phi = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi(); // on (u* p*)
            
            const auto& dphi_displacement = ExtractSubMatrix(grad_felt_phi, displacement_ref_felt);
            
            const auto& dphi_test_displacement = ExtractSubMatrix(test_grad_felt_phi, test_displacement_ref_felt);
            
            const auto& phi = infos_at_quad_pt_for_unknown.GetFEltPhi(); // on (u p)
            const auto& test_phi = infos_at_quad_pt_for_unknown.GetFEltPhi(); // on (u* p*)
            
            const auto& pressure_phi = ExtractSubVector(phi, pressure_ref_felt);
            const auto& test_pressure_phi = ExtractSubVector(test_phi, test_pressure_ref_felt);
            
            Wrappers::Seldon::Transpose(dphi_displacement, transposed_dphi_displacement);
            
            Advanced::OperatorNS::ComputeGradientDisplacementMatrix(infos_at_quad_pt_for_unknown,
                                                                    displacement_ref_felt,
                                                                    local_displacement,
                                                                    displacement_gradient);
            
            auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
            const auto& derivative_green_lagrange_at_quad_point = derivative_green_lagrange.Update(displacement_gradient);
            const auto& transposed_derivative_green_lagrange_at_quad_point = derivative_green_lagrange.GetTransposed();
            const auto& cauchy_green_tensor = GetCauchyGreenTensor();
            const auto& cauchy_green_tensor_at_quad_point = cauchy_green_tensor.GetValue(quad_pt, geom_elt);
            
            auto& invariant_holder = this->GetNonCstInvariantHolder();
            invariant_holder.Update(cauchy_green_tensor_at_quad_point, quad_pt, geom_elt);
            
            double pressure_at_quad_point = 0.;
            for (int node_index = 0; node_index < int_Nnode_pressure; ++node_index)
            {
                const unsigned int unsigned_node_index = static_cast<unsigned int>(node_index);
                
                pressure_at_quad_point += pressure_phi(node_index) * local_pressure[unsigned_node_index];
            }
            
            // TODO : replace by template
            const auto& dI3dC = invariant_holder.GetFirstDerivativeWrtCauchyGreen(invariant_holder_type::invariants_first_derivative_index::dI3dC);
            hydrostatic_stress.Copy(dI3dC);
            diff_hydrostatic_stress_wrt_pres.Copy(hydrostatic_stress);
            const double inv3 = Invariant3<FeltSpaceDimensionT>(cauchy_green_tensor_at_quad_point);
            const double sqrt_inv3 = std::sqrt(inv3);
            
            Seldon::Mlt(- pressure_at_quad_point / sqrt_inv3, hydrostatic_stress);
            Seldon::Mlt(transposed_derivative_green_lagrange_at_quad_point, hydrostatic_stress, rhs_disp);
            
            Seldon::Mlt(-1. / sqrt_inv3, diff_hydrostatic_stress_wrt_pres);
            
            const auto penalization_gradient = hydrostatic_law_->FirstDerivativeWThirdInvariant(invariant_holder,
                                                                                                quad_pt,
                                                                                                geom_elt);
            
            const double bulk = hydrostatic_law_->GetBulk().GetValue(quad_pt,geom_elt);
            const auto rhs_pres = penalization_gradient + pressure_at_quad_point / bulk;
            
            // Residual on disp
            for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
            {
                const auto dof_first_index = static_cast<int>(test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                const auto component_first_index = static_cast<int>(row_component * Ncomponent);
                
                // Compute the new contribution to vector_result here.
                // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                for (int row_node = 0; row_node < int_Nnode_disp; ++row_node)
                {
                    double value = 0.;
                    
                    for (int col = 0; col < int_Ncomponent; ++col)
                        value += dphi_test_displacement(row_node, col) * rhs_disp(col + component_first_index);
                    
                    vector_result(dof_first_index + row_node) += value * weight_meas;
                }
            }
            
            // Residual on pres
            const auto dof_first_index_pres = static_cast<int>(Nnode_disp * Ncomponent);
            for (int row_node = 0; row_node < int_Nnode_pressure; ++row_node)
            {
                vector_result(dof_first_index_pres + row_node) += rhs_pres * weight_meas * test_pressure_phi(row_node);
            }
            
            // Derivative disp disp
            const auto& d2I3dCdC = invariant_holder.GetSecondDerivativeWrtCauchyGreen(invariant_holder_type::invariants_second_derivative_index::d2I3dCdC);
            
            Wrappers::Seldon::OuterProd(dI3dC, dI3dC, hydrostatic_tangent);
            Seldon::Mlt(pressure_at_quad_point * std::pow(inv3, - 1.5), hydrostatic_tangent);
            
            Seldon::Add(- 2 * sqrt_inv3 * pressure_at_quad_point, d2I3dCdC, hydrostatic_tangent);
            
            Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeLinearPart(derivative_green_lagrange_at_quad_point,
                                transposed_derivative_green_lagrange_at_quad_point,
                                hydrostatic_tangent,
                                linear_part_intermediate_matrix,
                                // < internal quantity; better design
                                // would be to encapsulate it in
                                // an Internal class but I have no time
                                // to do this minor fix now.
                                linear_part);
            
            Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ComputeNonLinearPart<FeltSpaceDimensionT>(hydrostatic_stress, tangent_matrix_disp_disp);
            
            Seldon::Add(1., linear_part, tangent_matrix_disp_disp);
            
            //std::cout << "tangent_matrix_disp_disp"<< std::endl;
            //tangent_matrix_disp_disp.Print();
            
            // Derivative disp pres
            Seldon::Mlt(transposed_derivative_green_lagrange_at_quad_point, diff_hydrostatic_stress_wrt_pres, tangent_vector_disp_pres);
            
            //std::cout << "tangent_vector_disp_pres"<< std::endl;
            //tangent_vector_disp_pres.Print();
            
            // Derivative pres disp
            const auto penalization_scd_deriv = hydrostatic_law_->SecondDerivativeWThirdInvariant(invariant_holder,
                                                                                                  quad_pt,
                                                                                                  geom_elt);
            
            tangent_vector_pres_disp.Copy(tangent_vector_disp_pres);
            Seldon::Mlt(-2. * sqrt_inv3 * penalization_scd_deriv, tangent_vector_pres_disp);
            
            //std::cout << "tangent_vector_disp_pres"<< std::endl;
            //tangent_vector_disp_pres.Print();
            
            // Derivative pres pres
            const double tangent_matrix_pres_pres = weight_meas / bulk;
            
            // Tangent disp disp
            for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index = static_cast<int>(test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                
                for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
                {
                    const auto col_first_index = static_cast<int>(displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component));
                    
                    Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(tangent_matrix_disp_disp,
                                                                                    row_component,
                                                                                    col_component,
                                                                                    gradient_based_block);
                    
                    Seldon::Mlt(1., dphi_test_displacement,
                                gradient_based_block,
                                dphi_test_disp_mult_gradient_based_block);
                    
                    Seldon::Mlt(weight_meas,
                                dphi_test_disp_mult_gradient_based_block,
                                transposed_dphi_displacement,
                                block_contribution);
                    
                    for (int row_node = 0; row_node < int_Nnode_test_disp; ++row_node)
                    {
                        for (int col_node = 0u; col_node < int_Nnode_disp; ++col_node)
                            matrix_result(row_first_index + row_node, col_first_index + col_node)
                            += block_contribution(row_node, col_node);
                    }
                }
            }
            
            
            
            // Tangent disp pres
            const auto col_first_index_pressure = static_cast<int>(Ncomponent * Nnode_disp);
            const auto row_first_index_pressure = static_cast<int>(Ncomponent * Nnode_test_disp);
            
            for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index_disp = static_cast<int>(test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                
                ExtractGradientBasedBlockColumnMatrix(tangent_vector_disp_pres,
                                                      row_component,
                                                      column_matrix);
                
                Seldon::Mlt(weight_meas, dphi_test_displacement,
                            column_matrix,
                            dphi_test_disp_mult_column_matrix);
                
                for (int row_node = 0; row_node < int_Nnode_test_disp; ++row_node)
                {
                    for (int col_node = 0; col_node < int_Nnode_pressure; ++col_node)
                    {
                        block_contribution_disp_pres(row_node, col_node) =
                        dphi_test_disp_mult_column_matrix(row_node, 0) * pressure_phi(col_node);
                    }
                }
                
                for (int row_node = 0; row_node < int_Nnode_test_disp; ++row_node)
                {
                    for (int col_node = 0u; col_node < int_Nnode_pressure; ++col_node)
                        matrix_result(row_first_index_disp + row_node, col_first_index_pressure + col_node)
                        += block_contribution_disp_pres(row_node, col_node);
                }
            }
            
            
            // Tangent pres disp
            for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
            {
                const auto col_first_index_disp = static_cast<int>(displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component));
                
                ExtractGradientBasedBlockRowMatrix(tangent_vector_pres_disp,
                                                   col_component,
                                                   row_matrix);
                
                Seldon::Mlt(weight_meas, row_matrix,
                            transposed_dphi_displacement,
                            row_matrix_mult_transposed_dphi_disp);
                
                for (int row_node = 0; row_node < int_Nnode_test_pressure; ++row_node)
                {
                    for (int col_node = 0; col_node < int_Nnode_disp; ++col_node)
                    {
                        block_contribution_pres_disp(row_node, col_node) = test_pressure_phi(row_node) *
                        row_matrix_mult_transposed_dphi_disp(0, col_node);
                    }
                }
                
                for (int row_node = 0; row_node < int_Nnode_test_pressure; ++row_node)
                {
                    for (int col_node = 0u; col_node < int_Nnode_disp; ++col_node)
                        matrix_result(row_first_index_pressure + row_node, col_first_index_disp + col_node)
                        += block_contribution_pres_disp(row_node, col_node);
                }
            }
            
            // Tangent pres pres
            for (int row_node = 0; row_node < int_Nnode_test_pressure; ++row_node)
            {
                for (int col_node = 0u; col_node < int_Nnode_pressure; ++col_node)
                    matrix_result(row_first_index_pressure + row_node, col_first_index_pressure + col_node)
                    += tangent_matrix_pres_pres * test_pressure_phi(row_node) * pressure_phi(col_node);
            }
        }
        
        
        template <class HydrostaticLawPolicyT>
        inline const typename MixedSolidIncompressibility<HydrostaticLawPolicyT>::invariant_holder_type&
        MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::GetInvariantHolder() const noexcept
        {
            assert(!(!invariant_holder_));
            return *invariant_holder_;
        }
        
        
        
        template <class HydrostaticLawPolicyT>
        inline typename MixedSolidIncompressibility<HydrostaticLawPolicyT>::invariant_holder_type&
        MixedSolidIncompressibility<HydrostaticLawPolicyT>
        ::GetNonCstInvariantHolder() noexcept
        {
            return const_cast<typename MixedSolidIncompressibility<HydrostaticLawPolicyT>::invariant_holder_type&>(GetInvariantHolder());
        }
        

    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_MIXED_SOLID_INCOMPRESSIBILITY_HXX_
