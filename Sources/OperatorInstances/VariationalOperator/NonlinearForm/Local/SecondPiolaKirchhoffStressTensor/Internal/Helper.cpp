///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:35:04 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifdef MOREFEM_CHECK_NAN_AND_INF
# include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"
#endif // MOREFEM_CHECK_NAN_AND_INF

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
    
    
        namespace LocalVariationalOperatorNS
        {
            
            
            namespace SecondPiolaKirchhoffStressTensorNS
            {
                
                
                template<>
                void ComputeNonLinearPart<1>(const LocalVector& dW, LocalMatrix& out)
                {
                    out(0, 0) = dW(0); // Term SIGMA_xx
                }
                
            
                template<>
                void ComputeNonLinearPart<2>(const LocalVector& dW, LocalMatrix& out)
                {
                    for (int i = 0; i < 2; ++i)
                    {
                        int twice = 2 * i;
                        
                        for (int iComp = 0; iComp < 2; ++iComp)
                            out(twice + iComp, twice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy
                        
                        out(twice, twice + 1) = out(twice + 1, twice) = dW(2); // Term SIGMA_xy
                    }
                }
                
                
                
                template<>
                void ComputeNonLinearPart<3>(const LocalVector& dW, LocalMatrix& out)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        int thrice = 3 * i;
                        
                        for (int iComp = 0; iComp < 3; ++iComp)
                            out(thrice + iComp, thrice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy, SIGMA_zz
                        
                        out(thrice, thrice + 1) = out(thrice + 1, thrice) = dW(3); // Term SIGMA_xy
                        out(thrice + 1, thrice + 2) = out(thrice + 2, thrice + 1) = dW(4); // Term SIGMA_yz
                        out(thrice, thrice + 2) = out(thrice + 2, thrice) = dW(5); // Term SIGMA_xz
                    }
                    
                }
                
                
                void ComputeLinearPart(const LocalMatrix& De,
                                       const LocalMatrix& transposed_De,
                                       const LocalMatrix& d2W,
                                       LocalMatrix& linear_part_intermediate_matrix,
                                       LocalMatrix& linear_part)
                {
                    Seldon::Mlt(1., transposed_De, d2W, linear_part_intermediate_matrix);
                    Seldon::Mlt(1., linear_part_intermediate_matrix, De, linear_part);
                    
                    # ifdef MOREFEM_CHECK_NAN_AND_INF
                    ::MoReFEM::Wrappers::Seldon::ThrowIfNanInside(linear_part, __FILE__, __LINE__);
                    # endif // MOREFEM_CHECK_NAN_AND_INF
                }
                
            
            } // namespace SecondPiolaKirchhoffStressTensorNS
        
        
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
