///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                template<class InputParameterDataT>
                InputAnalyticalPrestress::InputAnalyticalPrestress(const InputParameterDataT& input_parameter_data,
                                                                   const Domain& domain)
                : initial_value_active_stress_(Utilities::InputParameterListNS::Extract<InputParameter::AnalyticalPrestress::InitialCondition::ActiveStress>::Value(input_parameter_data))
                {
                    contractility_ =
                        InitScalarParameterFromInputData<InputParameter::AnalyticalPrestress::Contractility>("Contractility",
                                                                                                                domain,
                                                                                                                input_parameter_data);
                };


                inline const ScalarParameter<>& InputAnalyticalPrestress::GetContractility() const noexcept
                {
                    assert(!(!contractility_));
                    return *contractility_;
                }


                inline double InputAnalyticalPrestress::GetInitialValueActiveStress() const noexcept
                {
                    return initial_value_active_stress_;
                }


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HXX_
