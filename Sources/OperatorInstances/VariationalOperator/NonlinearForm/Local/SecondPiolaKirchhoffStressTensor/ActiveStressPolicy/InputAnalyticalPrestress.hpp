///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/Base.hpp"

# include "Core/InputParameter/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"

# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                /*!
                 * \brief Policy to use when \a InputAnalyticalPrestress is involved in \a SecondPiolaKirchhoffStressTensor.
                 *
                 * \todo #9 (Gautier) Explain difference with AnalyticalPrestress.
                 */
                struct InputAnalyticalPrestress final
                {
                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = InputAnalyticalPrestress;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    /*!
                     * \brief Constructor.
                     *
                     * \copydoc doxygen_hide_input_parameter_data_arg
                     * \param[in] domain Domain upon which the operator is defined.
                     */
                    template <class InputParameterDataT>
                    explicit InputAnalyticalPrestress(const InputParameterDataT& input_parameter_data,
                                                      const Domain& domain);

                public:

                    //! Constant accessor on contractility.
                    const ScalarParameter<>& GetContractility() const noexcept;

                    //! Constant accessor on the initial value of the active stress.
                    double GetInitialValueActiveStress() const noexcept;

                private:

                    //! Contracitility.
                    ScalarParameter<>::unique_ptr contractility_ = nullptr;

                    //! Initial value of the active stress.
                    const double initial_value_active_stress_;

                };


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/InputAnalyticalPrestress.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_INPUT_ANALYTICAL_PRESTRESS_HPP_
