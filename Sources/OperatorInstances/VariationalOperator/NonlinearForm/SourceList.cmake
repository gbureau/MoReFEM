target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MixedSolidIncompressibility.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MixedSolidIncompressibility.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
