target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hpp"
)

