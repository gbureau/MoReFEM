///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 6 Oct 2017 11:34:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_HXX_


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {



        inline constexpr double ExponentialJ1J4::SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                                  const QuadraturePoint& quad_pt,
                                                                                  const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }



        inline constexpr double ExponentialJ1J4::SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                                          const QuadraturePoint& quad_pt,
                                                                                          const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4::SecondDerivativeWFirstAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                                                          const QuadraturePoint& quad_pt,
                                                                                          const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4::SecondDerivativeWSecondAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                                                           const QuadraturePoint& quad_pt,
                                                                                           const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetMu1() const noexcept
        {
            return mu_1_;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetMu2() const noexcept
        {
            return mu_2_;
        }

        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetC0() const noexcept
        {
            return c_0_;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetC1() const noexcept
        {
            return c_1_;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetC2() const noexcept
        {
            return c_2_;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetC3() const noexcept
        {
            return c_3_;
        }


        inline const ExponentialJ1J4::scalar_parameter& ExponentialJ1J4::GetBulk() const noexcept
        {
            return bulk_;
        }


        inline const FiberList<ParameterNS::Type::vector>* ExponentialJ1J4::GetFibers() const noexcept
        {
            return fibers_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_HXX_
