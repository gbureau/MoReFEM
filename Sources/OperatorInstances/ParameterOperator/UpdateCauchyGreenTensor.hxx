///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        inline void UpdateCauchyGreenTensor::Update(const GlobalVector& current_solid_displacement) const
        {
            return parent::UpdateImpl(current_solid_displacement);
        }


        inline void UpdateCauchyGreenTensor
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalParameterOperator& local_operator,
                                      const std::tuple<const GlobalVector&>& additional_args) const
        {
            const GlobalVector& solid_displacement = std::get<0>(additional_args);

            GlobalVariationalOperatorNS::ExtractLocalDofValues(local_felt_space,
                                                               this->GetExtendedUnknown(),
                                                               solid_displacement,
                                                               local_operator.GetNonCstIncrementLocalDisplacement());
        }


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_
