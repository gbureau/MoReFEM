///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Mar 2016 10:53:35 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_

# include <memory>

namespace MoReFEM
{


    namespace ParameterNS
    {

        /*!
         *
         * \class doxygen_hide_parameter_without_time_dependency
         *
         * At the moment the time dependency is not allowed (it is relatively easy to change that: just add a new
         * argument in the constructor and propagate it into the underlying Parameter. Only drawback is that the class
         * should become template with time dependancy functor as template argument, unless you impose the functor
         * to be something like std::function<double(double)>).
         */


        namespace TimeDependencyNS
        {


            /*!
             * \brief Policy to use when there are no time dependency for the \a Parameter.
             */
            template<Type TypeT>
            struct None
            {


                //! Convenient alias.
                using unique_ptr = std::unique_ptr<None>;

                //! Defined for conveniency; do nothing.
                void Update() { }

                //! Defined for conveniency; do nothing.
                void Update(double time) { static_cast<void>(time); }


            };


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_
