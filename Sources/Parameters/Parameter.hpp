///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_HPP_
# define MOREFEM_x_PARAMETERS_x_PARAMETER_HPP_

# include <memory>
# include <fstream>
# include <array>
# include <cassert>
# include <functional>

# include "Utilities/MatrixOrVector.hpp"
# include "Utilities/Filesystem/File.hpp"

# include "Parameters/ParameterType.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Mesh/Mesh.hpp"
# include "Geometry/Domain/Domain.hpp"

# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

# include "Parameters/TimeDependency/None.hpp"
# include "Parameters/TimeDependency/Internal/Dispatcher.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;
    class ScalarParameterFromFile;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \class doxygen_hide_param_time_dependancy
     *
     * A Parameter is first and foremost a spatial-dependant data: its main purpose is to describe the value
     * of a physical parameter at a given local position. However, we might also want to apply a decoupled time
     * dependancy, i.e. consider value of the parameter might be determined by:
     * \verbatim
     P(x, t) = f(x) * g(t)
     \endverbatim
     *
     * In this case, g(t) is stored as a function and is recomputed at each \a Parameter::TimeUpdate() calls (such calls
     * should therefore be located in Model::InitializeStep() where the time update actually occur).
     */


    /*!
     * \brief Abstract class used to define a Parameter.
     *
     * \tparam TypeT Type of the parameter (real, vector, matrix).
     *
     * \copydoc doxygen_hide_param_time_dependancy
     *
     * There are two distinct categories of \a Parameter:
     * - Those that are defined from the input parameter file. They might be constant, piece constant by domain or given
     * by an analytical Lua function, at the choice of the user of the model. They should be initialized through the
     * template functions provided in InitParameterFromInputData file (namely \a InitScalarParameterFromInputData
     * and \a InitThreeDimensionalParameterFromInputData).
     * - Parameters defined at dofs (that extract values from an underlying \a GlobalVector) or directly at quadrature points,
     * which are hardcoded in each model.
     *
     * Most of the instantiated classes actually derives from ParameterInstance, which itself derives from current
     * template class (but not all - see \a FiberList for a counter-example).
     */
    // \todo #0175 Comment LocalCoordsT
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    class Parameter
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = Parameter<TypeT, LocalCoordsT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to unique pointer to const object.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique pointer.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to traits.
        using traits = ParameterNS::Traits<TypeT>;

        //! Alias to return type.
        using return_type = typename traits::return_type;

    protected:

        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] name Name that will appear in outputs.
         * \param[in] domain Domain upon which the \a Parameter is defined. This domain must be restricted to a
         * \a Mesh.
         *
         * \tparam T Type of name, in forwarding reference idiom. It must be convertible to a std::string.
         *
         * \copydoc doxygen_hide_param_time_dependancy
         *
         */
        template<class T>
        explicit Parameter(T&& name,
                           const Domain& domain);


    public:

        //! Destructor.
        virtual ~Parameter() = default;

        //! Copy constructor.
        Parameter(const Parameter&) = delete;

        //! Move constructor.
        Parameter(Parameter&&) = delete;

        //! Copy affectation.
        Parameter& operator=(const Parameter&) = delete;

        //! Move affectation.
        Parameter& operator=(Parameter&&) = delete;

        ///@}

    public:


        //! Apply the time dependency if relevant.
        virtual void TimeUpdate();

        /*!
         * \brief Apply the time dependency if relevant.
         *
         * One should prefer to use the default one if one wants to use the current time.
         * Extra security to verify the synchro of the parameter to the current time is done in he default one.
         * This method is for particular cases only when the user knows exactly what is he doing.
         *
         * \param[in] time Time for the update.
         */
        virtual void TimeUpdate(double time);

        /*!
         * \brief Set the time dependency functor.
         *
         * This is relevant only for TimeDependencyT != TimeDependencyNS::None.
         *
         * \param[in] time_dependency Unique pointer to the time dependency object to set.
         */
        void SetTimeDependency(typename TimeDependencyT<TypeT>::unique_ptr&& time_dependency);


        /*!
         * \brief Get the value of the parameter at a given local position in a given \a geom_elt.
         *
         * \internal <b><tt>[internal]</tt></b> This method is actually called when IsConstant() yields false; if true
         * GetConstantValue() is called instead.
         *
         * \param[in] local_coords Local object at which the \a Parameter is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the value is computed.
         *
         * \return Value of the parameter.
         */
        return_type GetValue(const LocalCoordsT& local_coords, const GeometricElt& geom_elt) const;



        /*!
         * \brief Returns the constant value (if the parameter is constant).
         *
         * If not constant, an assert is raised (in debug mode).
         *
         * \return Constant value of the parameter.
         */
        return_type GetConstantValue() const;

        /*!
         * \brief Enables to modify the constant value of a parameter.
         *
         * \param[in] value Value to modify the constant value with.
         */
        virtual void SetConstantValue(double value) = 0;

        //! Whether the parameter varies spatially or not.
        virtual bool IsConstant() const = 0;

        /*!
         * \brief Write the content of the Parameter in a stream.
         *
         * In first draft the output format is up to the policy (maybe later we may prefer to write at all quadrature
         * points for all cases); the exact content is indeed defined in the virtual method SupplWrite(), to be defined
         * in each inherited classes.
         *
         * \param[in,out] out Stream onto which Parameter content may be written.
        */
        void Write(std::ostream& out) const;

        /*!
         * \brief Write the content of the Parameter in a file.
         *
         * This method calls the namesake method that writes on a stream.
         *
         * \param[in] filename Path to the file in which value will be written. The path must be valid (all directories
         * must exist) and if a namesake already exists it is overwritten.
         */
        void Write(const std::string& filename) const;

        //! Returns the \a Domain upon which the parameter is defined.
        const Domain& GetDomain() const noexcept;

        //! Whether the class is time-dependent or not.
        constexpr bool IsTimeDependent() const noexcept;

    public:

        /*!
         * \brief Constant accessor to the object which handles if relevant the time dependancy (computation of
         * the time related factor, etc...).
         *
         * Shouldn't be called very often...
         *
         * \return Time dependency object.
         */
        const TimeDependencyT<TypeT>& GetTimeDependency() const noexcept;


    protected:

        //! Name that will appear in outputs.
        const std::string& GetName() const;



    private:

        /*!
         * \brief Non constant accessor to the object which handles if relevant the time dependancy
         * (computation of the time related factor, etc...).
         */
        TimeDependencyT<TypeT>& GetNonCstTimeDependency() noexcept;


    private:


        /*!
         * \class doxygen_hide_parameter_suppl_get_constant_value
         *
         * \brief Returns the constant value (if the parameters is constant).
         *
         * If the \a Parameter gets a time dependency (which is of the form f(x) * g(t)), current method returns only
         * f(x). This method is expected to be called only in GetConstantValue() method, which adds up the g(t) contribution
         * \internal This choice also makes us respect a C++ idiom that recommends avoiding virtual public methods.
         *
         * \return Constant value of the \a Parameter. If the \a Parameter is not constant, this method should never be called.
         */

        //! copydoc doxygen_hide_parameter_suppl_get_constant_value
        virtual return_type SupplGetConstantValue() const = 0;



        /*!
         * \class doxygen_hide_parameter_suppl_get_any_value
         *
         * \brief Returns a stored value (Any: the point is actually to assert its type for some functions overload).
         *
         * \internal <b><tt>[internal]</tt></b> The point here is not the value itself, but the informations that
         * might be retrieved from it, such as number of rows and columns if TypeT == Type::matrix.
         *
         * \return Any value of the relevant type for the \a Parameter.
         */

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        virtual return_type SupplGetAnyValue() const = 0;


        /*!
         * \class doxygen_hide_parameter_suppl_get_value
         *
         * \brief Get the (spatially-only) value of the parameter at a given local position in a given \a geom_elt.
         *
         * If the \a Parameter gets a time dependency (which is of the form f(x) * g(t)), current method returns only
         * f(x). This method is expected to be called only in GetValue() method, which adds up the g(t) contribution
         * \internal This choice also makes us respect a C++ idiom that recommends avoiding virtual public methods.
         *
         * \param[in] geom_elt \a GeometricElt inside which the value is computed.
         *
         * \return Value of the parameter.
         */

        /*!
         * \class doxygen_hide_parameter_suppl_get_value_local_coords
         *
         * \copydoc doxygen_hide_parameter_suppl_get_value
         * \param[in] local_coords \a LocalCoords at which the value is computed.
         */

         /*!
         * \class doxygen_hide_parameter_suppl_get_value_quad_pt
         *
          * \copydoc doxygen_hide_parameter_suppl_get_value
         * \param[in] quad_pt \a QuadraturePoint at which the value is computed.
         */


        /*!
         * \copydoc doxygen_hide_parameter_suppl_get_value
         * \param[in] local_coords Local object at which the \a Parameter is evaluated.
         */
        virtual return_type SupplGetValue(const LocalCoordsT& local_coords, const GeometricElt& geom_elt) const = 0;

        /*!
         * \brief Write the content of the Parameter in a stream.
         *
         * \param[in,out] out Stream to which Parameter is written.
         */
        virtual void SupplWrite(std::ostream& out) const = 0;


        /*!
         * \class doxygen_hide_parameter_suppl_time_update
         *
         * \brief Add here any additional TimeUpdate that might be relevant.
         *
         * For instance, if a Parameter depends on other parameters, you must make sure those are correctly updated,
         * and possibly some internals might have to be recomputed.
         */

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        virtual void SupplTimeUpdate() = 0;

    private:

        /*!
         * \brief Apply the time dependency factor to the spatially computed value.
         *
         * Remember we assume a parameter may include a time dependency of the form P = f(x) * g(t).
         *
         * \internal Should be called only within GetValue() and GetConstantValue() methods.
         *
         * If there are no time dependency, the argument is returned as such.
         *
         * \param[in] value_without_time_factor Value computed because the time factor was applied (f(x) in formula above).
         * \return Value once the time factor was applied.
         */
        return_type ApplyTimeDependancyFactor(return_type value_without_time_factor) const;


        /*!
         * \class doxygen_hide_parameter_suppl_time_update_with_time
         *
         * \brief Add here any additional TimeUpdate that might be relevant.
         *
         * For instance, if a Parameter depends on other parameters, you must make sure those are correctly updated,
         * and possibly some internals might have to be recomputed.
         *
         * One should prefer to use the default one if one wants to use the current time.
         * Extra security to verify the synchro of the parameter to the current time is done in he default one.
         * This method is for particular cases only when the user knows exactly what is he doing.
         *
         * \param[in] time Time for the update.
         */

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        virtual void SupplTimeUpdate(double time) = 0;


    private:

        //! Name that will appear in outputs.
        std::string name_;

        //! Domain upon which the parameter is defined. Must be consistent with mesh.
        const Domain& domain_;

        /*!
         * \brief Object which handles if relevant the time dependancy (computation of the time related factor,
         * etc...).
         */
        typename TimeDependencyT<TypeT>::unique_ptr time_dependency_ = nullptr;
    };


    //! Convenient alias to define a scalar parameter.
    template
    <
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    using ScalarParameter = Parameter<ParameterNS::Type::scalar, LocalCoords, TimeDependencyT>;


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Parameter.hxx"


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_HPP_
