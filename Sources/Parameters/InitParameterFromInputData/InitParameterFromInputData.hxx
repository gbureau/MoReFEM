///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
# define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_



namespace MoReFEM
{


    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class T,
        class InputParameterDataT
    >
    typename ScalarParameter<TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(T&& name,
                                     const Domain& domain,
                                     const InputParameterDataT& input_parameter_data)
    {
        namespace IPL = Utilities::InputParameterListNS;

        decltype(auto) nature =
            IPL::Extract<typename ParameterT::Nature>::Value(input_parameter_data);

        decltype(auto) scalar_value =
            IPL::Extract<typename ParameterT::Scalar>::Value(input_parameter_data);

        decltype(auto) domain_id =
            IPL::Extract<typename ParameterT::PiecewiseConstantByDomainId>::Value(input_parameter_data);

        decltype(auto) value_by_domain =
            IPL::Extract<typename ParameterT::PiecewiseConstantByDomainValue>::Value(input_parameter_data);

        if (domain_id.size() != value_by_domain.size())
        {
            throw Exception("The number of domain id given in the input file is different from the number of value "
                            "for the piecewise constant by domain parameter.",
                            __FILE__,
                            __LINE__);
        }

        return Internal::ParameterNS::InitScalarParameterFromInputData
                <
                    typename ParameterT::LuaFunction,
                    TimeDependencyT
                >(std::forward<T>(name),
                  domain,
                  input_parameter_data,
                  nature,
                  scalar_value,
                  domain_id,
                  value_by_domain);
    }


    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class T,
        class InputParameterDataT
    >
    typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    InitThreeDimensionalParameterFromInputData(T&& name,
                                               const Domain& domain,
                                               const InputParameterDataT& input_parameter_data)
    {
        namespace IPL = Utilities::InputParameterListNS;

        using nature_type = typename ParameterT::Nature;

        decltype(auto) nature_list =
            IPL::Extract<nature_type>::Value(input_parameter_data);

        if (nature_list.size() != 3)
            throw Exception("Exactly three items were expected for " + nature_type::enclosing_section::GetFullName() + "::"
                            + nature_type::NameInFile(),
                            __FILE__, __LINE__);

        {
            auto it = std::find(nature_list.cbegin(), nature_list.cend(), "ignore");

            if (it != nature_list.cend())
            {
                if (std::count(nature_list.cbegin(), nature_list.cend(), "ignore") != 3)
                    throw Exception("Error for " + nature_type::enclosing_section::GetFullName() + "::" + nature_type::NameInFile() + ": if "
                                    "one of the item is 'ignore' the other ones should be 'ignore' as well.",
                                    __FILE__, __LINE__);

                return nullptr;
            }
        }

        using scalar_type = typename ParameterT::Scalar;

        decltype(auto) component_value_list =
            IPL::Extract<scalar_type>::Value(input_parameter_data);

        if (component_value_list.size() != 3)
            throw Exception("Exactly three items were expected for " + scalar_type::enclosing_section::GetFullName() + "::"
                            + scalar_type::NameInFile(),
                            __FILE__, __LINE__);

        std::vector<unsigned int> domain_id;
        std::vector<double> value_by_domain;

        auto&& component_x =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                typename ParameterT::LuaFunctionX,
                ParameterNS::TimeDependencyNS::None
            >("Lua function X",
              domain,
              input_parameter_data,
              nature_list[0],
              component_value_list[0],
              domain_id,
              value_by_domain);


        auto&& component_y =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                typename ParameterT::LuaFunctionY,
                ParameterNS::TimeDependencyNS::None
            >("Lua function Y",
              domain,
              input_parameter_data,
              nature_list[1],
              component_value_list[1],
              domain_id,
              value_by_domain);


        auto&& component_z =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                typename ParameterT::LuaFunctionZ,
                ParameterNS::TimeDependencyNS::None
            >("Lua function Z",
              domain,
              input_parameter_data,
              nature_list[2],
              component_value_list[2],
              domain_id,
              value_by_domain);


        return std::make_unique<ParameterNS::ThreeDimensionalParameter<TimeDependencyT>>(std::forward<T>(name),
                                                                                         std::move(component_x),
                                                                                         std::move(component_y),
                                                                                         std::move(component_z));
    }


    template<>
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class StringT,
        class InputParameterDataT
    >
    inline typename Parameter<ParameterNS::Type::scalar, LocalCoords, TimeDependencyT>::unique_ptr
    InitParameterFromInputData<ParameterNS::Type::scalar>::Perform(StringT&& name,
                                                                   const Domain& domain,
                                                                   const InputParameterDataT& input_parameter_data)
    {
        return InitScalarParameterFromInputData<ParameterT, TimeDependencyT>(name,
                                                                             domain,
                                                                             input_parameter_data);
    }


    template<>
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class StringT,
        class InputParameterDataT
    >
    inline typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    InitParameterFromInputData<ParameterNS::Type::vector>::Perform(StringT&& name,
                                                                   const Domain& domain,
                                                                   const InputParameterDataT& input_parameter_data)
    {
        return InitThreeDimensionalParameterFromInputData<ParameterT, TimeDependencyT>(name,
                                                                                       domain,
                                                                                       input_parameter_data);
    }


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
