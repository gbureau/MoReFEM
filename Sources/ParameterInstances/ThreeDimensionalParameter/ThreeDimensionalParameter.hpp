///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 28 May 2015 14:52:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HPP_

# include <memory>
# include <vector>

# include "Parameters/Parameter.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        /*!
         * \brief Class to handle a 3D parameter (for instance a force).
         *
         * Such objects should in most if not all cases be initialized with InitThreeDimensionalParameter() free function.
         *
         * This is actually an aggregate of three \a Parameter that define current one.
         */
        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        class ThreeDimensionalParameter final
        : public Parameter
        <
            ParameterNS::Type::vector,
            LocalCoords,
            TimeDependencyT
        >
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = ThreeDimensionalParameter;

            //! \copydoc doxygen_hide_parameter_local_coords_type
            using local_coords_type = LocalCoords;

            //! Alias to base class.
            using parent = Parameter
            <
                ParameterNS::Type::vector,
                LocalCoords,
                TimeDependencyT
            >;

            //! Alias to return type.
            using return_type = typename parent::return_type;

            //! Alias to traits of parent class.
            using traits = typename parent::traits;

            //! Alias to scalar parameter.
            using scalar_parameter = ScalarParameter<TimeDependencyNS::None>;

            //! Alias to scalar parameter unique_ptr.
            using scalar_parameter_ptr = scalar_parameter::unique_ptr;

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            template<class T>
            explicit ThreeDimensionalParameter(T&& name,
                                               scalar_parameter_ptr&& x_component,
                                               scalar_parameter_ptr&& y_component,
                                               scalar_parameter_ptr&& z_component);

            //! Destructor.
            ~ThreeDimensionalParameter() override = default;

            //! Copy constructor.
            ThreeDimensionalParameter(const ThreeDimensionalParameter&) = delete;

            //! Move constructor.
            ThreeDimensionalParameter(ThreeDimensionalParameter&&) = delete;

            //! Copy affectation.
            ThreeDimensionalParameter& operator=(const ThreeDimensionalParameter&) = delete;

            //! Move affectation.
            ThreeDimensionalParameter& operator=(ThreeDimensionalParameter&&) = delete;

            ///@}

            /*!
             * \brief Write the content of the Parameter in a stream.
             */
            void SupplWrite(std::ostream& out) const override;

            //! \copydoc doxygen_hide_parameter_suppl_time_update
            void SupplTimeUpdate() override;

            //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
            void SupplTimeUpdate(double time) override;

            /*!
             *
             * \copydoc doxygen_hide_parameter_suppl_get_any_value
             */
            return_type SupplGetAnyValue() const override;

            /*!
             * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
             */
            void SetConstantValue(double value) override;

        private:


            //! copydoc doxygen_hide_parameter_suppl_get_constant_value
            return_type SupplGetConstantValue() const override;

            //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
            return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;

            //! Whether the parameter varies spatially or not.
            bool IsConstant() const override;


        private:

            //! Access to contribution of component x.
            scalar_parameter& GetScalarParameterX() const;

            //! Access to contribution of component y.
            scalar_parameter& GetScalarParameterY() const;

            //! Access to contribution of component z.
            scalar_parameter& GetScalarParameterZ() const;


        private:

            //! Contribution of the x component to the vectorial parameter.
            scalar_parameter_ptr scalar_parameter_x_;

            //! Contribution of the y component to the vectorial parameter.
            scalar_parameter_ptr scalar_parameter_y_;

            //! Contribution of the z component to the vectorial parameter.
            scalar_parameter_ptr scalar_parameter_z_;

            //! Content of the parameter.
            mutable LocalVector content_;


        };


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/ThreeDimensionalParameter/ThreeDimensionalParameter.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_PARAMETER_HPP_
