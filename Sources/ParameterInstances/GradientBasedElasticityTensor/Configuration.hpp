///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 11 Oct 2016 14:00:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HPP_

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        //! This enum encompasses the possible configurations to consider in a GradientBasedElasticityTensor.
        enum class GradientBasedElasticityTensorConfiguration
        {
            dim1,
            dim2_plane_stress,
            dim2_plane_strain,
            dim3
        };




        /*!
         * \brief Read the input parameter file to decide which configuration should be chosen for the
         * GradientBasedElasticityTensor.
         *
         * \copydoc doxygen_hide_input_parameter_data_arg
         * \param[in] mesh_dimension DImension of the mesh.
         *
         * \return Enum that specified the configuration to use in the model.
         */
        template<class InputParameterDataT>
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration
        ReadGradientBasedElasticityTensorConfigurationFromFile(const unsigned int mesh_dimension,
                                                               const InputParameterDataT& input_parameter_data);




    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HPP_
