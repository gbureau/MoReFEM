///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 15:35:41 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_

# include <memory>
# include <vector>

# include "Parameters/Parameter.hpp"
# include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"
# include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"



namespace MoReFEM
{


    namespace ParameterNS
    {


        /*!
         * \brief Gradient based elasticity tensor.
         *
         * \copydoc doxygen_hide_parameter_without_time_dependency
        */
        template
        <
            GradientBasedElasticityTensorConfiguration ConfigurationT
        >
        class GradientBasedElasticityTensor final
        : public Parameter<ParameterNS::Type::matrix, LocalCoords, TimeDependencyNS::None>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = GradientBasedElasticityTensor<ConfigurationT>;

            //! \copydoc doxygen_hide_parameter_local_coords_type
            using local_coords_type = LocalCoords;

            //! Alias to base class.
            using parent = Parameter<ParameterNS::Type::matrix, LocalCoords, TimeDependencyNS::None>;

            //! Alias to scalar parameter with no time dependency.
            using scalar_parameter = ScalarParameter<TimeDependencyNS::None>;

            //! Alias to return type.
            using return_type = typename parent::return_type;

            //! Alias to traits of parent class.
            using traits = typename parent::traits;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit GradientBasedElasticityTensor(const scalar_parameter& young_modulus,
                                                   const scalar_parameter& poisson_ratio);

            //! Destructor.
            ~GradientBasedElasticityTensor() override = default;

            //! Copy constructor.
            GradientBasedElasticityTensor(const GradientBasedElasticityTensor&) = delete;

            //! Move constructor.
            GradientBasedElasticityTensor(GradientBasedElasticityTensor&&) = delete;

            //! Copy affectation.
            GradientBasedElasticityTensor& operator=(const GradientBasedElasticityTensor&) = delete;

            //! Move affectation.
            GradientBasedElasticityTensor& operator=(GradientBasedElasticityTensor&&) = delete;

            ///@}

            /*!
             * \brief Write the content of the Parameter in a stream.
             */
            void SupplWrite(std::ostream& out) const override;

            /*!
             * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
             */
            void SetConstantValue(double value) override;

        private:

            //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
            return_type SupplGetValue(const local_coords_type& local_coords,
                                      const GeometricElt& geom_elt) const override;


            /*
             * \brief Whether the parameter varies spatially or not.
             */
            bool IsConstant() const override;

            //! Young modulus.
            const scalar_parameter& GetYoungModulus() const;

            //! Poisson coefficient.
            const scalar_parameter& GetPoissonRatio() const;

            //! Compute the current value.
            return_type ComputeValue(double young_modulus, double poisson_ratio) const;

            //! Returns the constant value (and compute it if first call).
            return_type SupplGetConstantValue() const override;

            //! \copydoc doxygen_hide_parameter_suppl_time_update
            void SupplTimeUpdate() override;

            //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
            void SupplTimeUpdate(double time) override;

            /*!
             *
             * \copydoc doxygen_hide_parameter_suppl_get_any_value
             */
            return_type SupplGetAnyValue() const override;

        private:

            //! Young modulus.
            const scalar_parameter& young_modulus_;

            //! Poisson coefficient.
            const scalar_parameter& poisson_ratio_;

            /*!
             * \brief Storage of the matrix.
             *
             * The matrix is allocated once in the initialization phase of the code; its content might change
             * during a call to GetValue() if either Young modulus or Poisson coefficient is not constant.
             */
            mutable Internal::ParameterNS::ComputeGradientBasedElasticityTensor<ConfigurationT> helper_;

        };


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
