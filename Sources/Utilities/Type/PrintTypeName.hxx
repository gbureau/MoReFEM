//! \file
//
//
//  PrintTypeName.hxx
//  MoReFEM
//
//  Created by sebastien on 03/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HXX_
# define MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HXX_


namespace MoReFEM
{


    template <class T>
    constexpr std::string_view GetTypeName()
    {
        std::string_view p = __PRETTY_FUNCTION__;

        # ifdef __clang__
        constexpr auto magic_number = 45ul;
        return std::string_view(p.data() + magic_number, p.size() - magic_number - 1);
        # elif defined(__GNUC__)
        constexpr auto magic_number = 60ul;
        return std::string_view(p.data() + magic_number, p.find(';', magic_number) - magic_number);
        # endif
    }


} // namespace MoReFEM


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HXX_
