///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 5 Nov 2015 11:00:08 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HPP_

# include <array>

# include "Utilities/Miscellaneous.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Function to set to nullptr all the content of a defined array.
         *
         * e.g.
         * \code
         * std::array<GlobalVector::unique_ptr, 5> = Utilities::NullptrArray<GlobalVector::unique_ptr, 5>();
         * \endcode
         *
         * in a class declaration of data attribute will init it with 5 nullptr.
         *
         * \return Array which includes \a N nullptr terms.
         */
        template<class ItemPtrT, std::size_t N>
        std::array<ItemPtrT, N> NullptrArray();


        /*!
         * \brief Helper struct to return statically the size of an array.
         *
         * Only the specialization matters; the generic declaration is intentionally left undefined.
         *
         */
        template<class T>
        struct ArraySize;


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        template<class T, std::size_t N>
        struct ArraySize<std::array<T, N>>
        {

            static constexpr std::size_t GetValue() noexcept;

        };


        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================




    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Containers/Array.hxx"


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_ARRAY_HPP_
