///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Impl
            {



                template<class TupleEltTypeT>
                template<class T>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     LuaOptionFile* lua_option_file,
                                                     Utilities::Type2Type<T>)
                {
                    assert(lua_option_file != nullptr);
                    element.SetValue(lua_option_file->Read<typename TupleEltTypeT::return_type>(fullname,
                                                                                                TupleEltTypeT::Constraint(),
                                                                                                __FILE__, __LINE__));
                }


                template<class TupleEltTypeT>
                template<class T, typename ...Args>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     LuaOptionFile* lua_option_file,
                                                     Utilities::Type2Type<Utilities::InputParameterListNS::LuaFunction<T(Args...)>>)
                {
                    assert(lua_option_file != nullptr);
                    Utilities::InputParameterListNS::LuaFunction<T(Args...)> lua_function(fullname, lua_option_file);
                    element.SetValue(std::move(lua_function));

                }


                template<class TupleEltTypeT>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     LuaOptionFile* lua_option_file)
                {
                    assert(lua_option_file != nullptr);
                    Utilities::Type2Type<typename TupleEltTypeT::return_type> t;
                    Set(element, fullname, lua_option_file, t);
                }


            } // namespace Impl


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
