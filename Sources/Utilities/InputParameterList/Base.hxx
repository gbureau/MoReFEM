///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {



            template<class DerivedT, class TupleT>
            Base<DerivedT, TupleT>::Base(const std::string& filename,
                                         const Wrappers::Mpi& mpi,
                                         DoTrackUnusedFields do_track_unused_fields)
            : mpi_parent(mpi),
            lua_option_file_(nullptr),
            input_parameter_file_(filename)
            {
                int is_initialized;

                MPI_Initialized(&is_initialized);

                if (!is_initialized)
                    throw ExceptionNS::MpiNotInitialized(__FILE__, __LINE__);

                static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "Template argument is expected to be a std::tuple.");

                CheckNoDuplicateKeysInTuple();

                // Init Lua option file.
                lua_option_file_ = std::make_unique<LuaOptionFile>(filename, __FILE__, __LINE__); 

                CheckUnboundInputParameter(filename, do_track_unused_fields);

                // Fill from the file all the objects in tuple_.
                Internal::InputParameterListNS::FillTuple<TupleT>(lua_option_file_.get(), tuple_);
            }


            template<class DerivedT, class TupleT>
            Base<DerivedT, TupleT>::~Base()
            { }

            template<class DerivedT, class TupleT>
            constexpr std::size_t Base<DerivedT, TupleT>::Size()
            {
                return std::tuple_size<TupleT>::value;
            }


            template<class DerivedT, class TupleT>
            inline const std::string&  Base<DerivedT, TupleT>::GetInputFile() const
            {
                assert(!input_parameter_file_.empty());
                return input_parameter_file_;
            }


            template<class DerivedT, class TupleT>
            const auto& Base<DerivedT, TupleT>::GetTuple() const
            {
                return tuple_;
            }


            template<class DerivedT, class TupleT>
            template<class InputParameterT, CountAsUsed CountAsUsedT>
            typename Utilities::ConstRefOrValue<typename InputParameterT::return_type>::type Base<DerivedT, TupleT>
            ::ReadHelper() const
            {
                const InputParameterT* parameter_ptr = nullptr;

                constexpr bool found = tuple_iteration::template Find<InputParameterT>();
                static_assert(found, "InputParameter not defined in the tuple!");

                tuple_iteration::template ExtractValue<InputParameterT>(tuple_, parameter_ptr);

                assert(!(!parameter_ptr) && "If the parameter is not defined in the tuple static assert two lines "
                       "earlier should have been triggered");

                const auto& parameter = *parameter_ptr;

                if (CountAsUsedT == CountAsUsed::yes)
                    parameter.SetAsUsed();

                return parameter.GetTheValue();
            }


            template<class DerivedT, class TupleT>
            template<class InputParameterT, CountAsUsed CountAsUsedT>
            typename Utilities::ConstRefOrValue<typename InputParameterT::return_type::value_type>::type
            Base<DerivedT, TupleT>::ReadHelper(unsigned int index) const
            {
                const auto& buf = ReadHelper<InputParameterT, CountAsUsedT>();
                assert(index < buf.size());
                return buf[index];
            }


            template<class DerivedT, class TupleT>
            template<class InputParameterT, UnexistentFolderPolicy UnexistentFolderPolicyT>
            std::string Base<DerivedT, TupleT>::ReadHelperFolder() const
            {
                std::string folder_name = ReadHelperPath<InputParameterT>();

                if (!FilesystemNS::Folder::DoExist(folder_name))
                {
                    switch(UnexistentFolderPolicyT)
                    {
                        case UnexistentFolderPolicy::create:
                        {
                            try
                            {
                                FilesystemNS::Folder::Create(folder_name, __FILE__, __LINE__);
                            }
                            catch(const Exception& )
                            {
                                // In parallel, when several ranks share the same filesystem, creation might fail
                                // due to the folder already existing (because created by another rank since the
                                // existence test failure above). So if we test it again here and find it already
                                // exists, we can safely dismiss the exception.
                                if (!FilesystemNS::Folder::DoExist(folder_name))
                                    throw;
                            }


                            break;
                        }
                        case UnexistentFolderPolicy::throw_exception:
                        {
                            throw ExceptionNS::FolderDoesntExist(folder_name, __FILE__, __LINE__);
                        }
                    }
                }

                return folder_name;
            }


            template<class DerivedT, class TupleT>
            template<class InputParameterT>
            std::string Base<DerivedT, TupleT>::ReadHelperPath() const
            {
                return Utilities::EnvironmentNS::SubstituteValues(ReadHelper<InputParameterT, CountAsUsed::yes>());
            }


            template<class DerivedT, class TupleT>
            template<class InputParameterT, CountAsUsed CountAsUsedT>
            unsigned int Base<DerivedT, TupleT>::ReadHelperNumber() const
            {
                return static_cast<unsigned int>(ReadHelper<InputParameterT, CountAsUsedT>().size());
            }



            template<class TupleT>
            void CreateDefaultInputFile(const std::string& path)
            {
                std::ofstream out;
                FilesystemNS::File::Create(out, path, __FILE__, __LINE__);

                out << "-- Comment lines are introduced by \"--\".\n";
                out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";

                Internal::InputParameterListNS::PrepareDefaultEntries<TupleT>(out);
            }




            template<class DerivedT, class TupleT>
            void Base<DerivedT, TupleT>::CheckNoDuplicateKeysInTuple() const
            {
                // Check there are no type duplicated in the tuple.
                Utilities::Tuple::AssertNoDuplicate<TupleT>::Perform();

                // Check there are no duplicated keys in the tuple (two different types that share the same key for instance).
                std::unordered_set<std::string> buf;
                tuple_iteration::CheckNoDuplicateKeysInTuple(buf);
            }



            template<class DerivedT, class TupleT>
            void Base<DerivedT, TupleT>::CheckUnboundInputParameter(const std::string& filename,
                                                        DoTrackUnusedFields do_track_unused_fields) const
            {
                // Check there are no parameters undefined in the tuple.
                if (do_track_unused_fields == DoTrackUnusedFields::yes)
                {
                    decltype(auto) lua_option_file = GetLuaOptionFile();

                    decltype(auto) entry_key_list = lua_option_file.GetEntryKeyList();

                    for (decltype(auto) entry_key : entry_key_list)
                    {
                        const auto pos = entry_key.rfind(".");

                        std::string_view section_name, variable;

                        if (pos == std::string::npos)
                        {
                            section_name = "";
                            variable = entry_key;
                        }
                        else
                        {
                            section_name = std::string_view(entry_key.data(), pos);
                            variable = std::string_view(&entry_key.at(pos + 1), entry_key.size() - pos - 1);
                        }


                        if (!tuple_iteration::DoMatchIdentifier(section_name, variable))
                            throw ExceptionNS::UnboundInputParameter(filename, section_name, variable,
                                                                     __FILE__, __LINE__);
                    }
                }
            }


            template<class DerivedT, class TupleT>
            template<class SubTupleT>
            void Base<DerivedT, TupleT>::EnsureSameLength() const
            {
                enum { size = std::tuple_size<SubTupleT>::value };
                std::size_t length;
                Internal::InputParameterListNS::ThroughSubtuple<SubTupleT, 0, size, TupleT>
                ::EnsureSameLength(tuple_, length);
            }


            template<class DerivedT, class TupleT>
            void Base<DerivedT, TupleT>::PrintUnused(std::ostream& out) const
            {
                // First gather the data from all processors: it is possible (though unlikely) than a given
                // input parameter is read only in some of the processors.
                std::vector<bool> stats;
                std::vector<std::string> identifiers;

                tuple_iteration::IsUsed(tuple_, identifiers, stats);

                assert(identifiers.size() == stats.size());

                // Then reduce it on the root processor.
                const auto& mpi = mpi_parent::GetMpi();

                auto&& reduced_stats = mpi.ReduceOnRootProcessor(stats, Wrappers::MpiNS::Op::LogicalOr);
                assert(reduced_stats.size() == stats.size());

                if (mpi.IsRootProcessor() && !std::all_of(reduced_stats.cbegin(), reduced_stats.cend(),
                                                          [](bool arg) { return arg; }))
                {
                    out << "Note about InputParameterList: some of the input parameters weren't "
                    "actually used by the program:" << std::endl;

                    const auto size = identifiers.size();

                    for (std::size_t i = 0ul; i < size; ++i)
                    {
                        if (!reduced_stats[i])
                            out << "\t - " << identifiers[i] << '\n';
                    }
                }
            }


            template<class ObjectT>
            template<class InputParameterDataT, CountAsUsed CountAsUsedT>
            typename ConstRefOrValue<typename Extract<ObjectT>::return_type>::type
            Extract<ObjectT>::Value(const InputParameterDataT& input_parameter_list)
            {
                return input_parameter_list.template ReadHelper<ObjectT, CountAsUsedT>();
            }


            template<class ObjectT>
            template<class InputParameterDataT>
            std::string Extract<ObjectT>::Path(const InputParameterDataT& input_parameter_list)
            {
                return input_parameter_list.template ReadHelperPath<ObjectT>();
            }



            template<class ObjectT>
            template<class InputParameterDataT, UnexistentFolderPolicy UnexistentFolderPolicyT>
            std::string Extract<ObjectT>
            ::Folder(const InputParameterDataT& input_parameter_list)
            {
                return input_parameter_list.template ReadHelperFolder<ObjectT, UnexistentFolderPolicyT>();
            }


            template<class ObjectT>
            template<class InputParameterDataT>
            unsigned int Extract<ObjectT>::Number(const InputParameterDataT& input_parameter_list)
            {
                return input_parameter_list.template ReadHelperNumber<ObjectT>();
            }


            template<class ObjectT>
            template<class InputParameterDataT, CountAsUsed CountAsUsedT>
            decltype(auto)
            Extract<ObjectT>::Subscript(const InputParameterDataT& input_parameter_list,
                                        unsigned int index)
            {
                return input_parameter_list.template ReadHelper<ObjectT, CountAsUsedT>(index);
            }


            template<class DerivedT, class TupleT>
            const LuaOptionFile& Base<DerivedT, TupleT>::GetLuaOptionFile() const noexcept
            {
                assert(!(!lua_option_file_));
                return *lua_option_file_;
            }


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_BASE_HXX_
