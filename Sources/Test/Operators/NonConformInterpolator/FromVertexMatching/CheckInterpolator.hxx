/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HXX_
# define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HXX_


namespace MoReFEM
{


    namespace TestVertexMatchingNS
    {



    } // namespace TestVertexMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HXX_
