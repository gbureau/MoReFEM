/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define CATCH_CONFIG_MAIN
#include "Test/Tools/Fixture.hpp"

#include "Test/Operators/VariationalInstances/Pk2/Model.hpp"
#include "Test/Operators/VariationalInstances/Pk2/InputParameterList.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::Fixture
        <
            TestNS::Pk2::Model,
            TestNS::Pk2::InputParameterList,
            LuaFile
        >;


} // namespace anonymous



TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical")
{
    GetModel().SameUnknown();
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
{
    GetModel().UnknownP1TestP1();
}


TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
{
    GetModel().UnknownP2TestP1();
}


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        return
            Utilities::EnvironmentNS::SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                       "VariationalInstances/Pk2/"
                                                       "demo_input_parameter_3D.lua");
    }

} // namespace anonymous

