//! \file 
//
//
//  main.cpp
//  MoReFEM
//
//  Created by sebastien on 04/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#define CATCH_CONFIG_MAIN
#include "ThirdParty/Source/Catch/catch.hpp"

#include "Utilities/Type/PrintTypeName.hpp"


using namespace MoReFEM;


TEST_CASE("Types are correctly extracted" )
{
    REQUIRE(GetTypeName<int>() == "int");
    REQUIRE(GetTypeName<double>() == "double");

    #ifdef __clang__
    constexpr auto expected = "const int &";
    # elif defined(__GNUC__)
    constexpr auto expected = "const int&";
    #endif

    REQUIRE(GetTypeName<const int&>() == expected);
    REQUIRE(GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double> >");
    REQUIRE(GetTypeName<std::string>() == "std::__1::basic_string<char>");
}


