//! \file
//
//
//  Fixture.hpp
//  MoReFEM
//
//  Created by sebastien on 16/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_

# include <memory>
# include <vector>

# include "Test/Tools/InitializeTestMoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \brief An helper class to build tests with catch.
         *
         * The goal is to provide in a main different tests:
         *
         * \code
         namespace // anonymous
         {
            struct LuaFile
            {
                static std::string GetPath();
            };

            using fixture_type =
                TestNS::Fixture
                <
                    TestNS::Pk2::Model,
                    TestNS::Pk2::InputParameterList,
                    LuaFile
                >;
         } // namespace anonymous

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are identical")
         {
            GetModel().SameUnknown();
         }

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different but share the same P1 shape function label")
         {
            GetModel().UnknownP1TestP1();
         }

         TEST_CASE_METHOD(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test function P1")
         {
            GetModel().UnknownP2TestP1();
         }
         * \endcode
         *
         * The fixture here is a hack to run once and only once most of the initialization steps (MPI initialization,
         * building of the different singletons, etc...).
         *
         * Basically it nuilds and run the \a Model throughout all of the integration tests.
         *
         * \attention Contrary to the original intent of the fixture, it does NOT make tabula rasa of the state after
         * each test. So the tests should be a independant as possible from one another.
         *
         * \tparam ModelT Type of the model to build.
         * \tparam InputParameterListT Type of the \a InputParameterList used along with the \a Model.
         * \tparam LuaFileT An ad hoc class which encapsulates in a \a GetPath() method the path to the Lua file.
         */
        template
        <
            class ModelT,
            class InputParameterListT,
            class LuaFileT
        >
        struct Fixture
        {
        public:

            //! Alias to the proper InitializeTestMoReFEMData type.
            using initialize_type = TestNS::InitializeTestMoReFEMData<InputParameterListT>;

            /*!
             * \brief Static method which yields the model considered for the tests.
             *
             * \return Constant reference to the \a Model.
             */
            static const ModelT& GetModel();

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Fixture() = default;

            //! Destructor.
            ~Fixture() = default;

            //! Copy constructor.
            Fixture(const Fixture&) = delete;

            //! Move constructor.
            Fixture(Fixture&&) = delete;

            //! Copy affectation.
            Fixture& operator=(const Fixture&) = delete;

            //! Move affectation.
            Fixture& operator=(Fixture&&) = delete;

            ///@}




        };


    } // namespace TestNS


} // namespace MoReFEM


# include "Test/Tools/Fixture.hxx"


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HPP_
