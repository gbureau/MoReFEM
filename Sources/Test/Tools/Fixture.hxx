//! \file
//
//
//  Fixture.hxx
//  MoReFEM
//
//  Created by sebastien on 16/03/2018.
//Copyright © 2018 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_


namespace MoReFEM
{


    namespace TestNS
    {


        template
        <
            class ModelT,
            class InputParameterListT,
            class LuaFileT
        >
        const ModelT& Fixture<ModelT, InputParameterListT, LuaFileT>::GetModel()
        {
            static bool first_call = true;

            static initialize_type init(LuaFileT::GetPath());

            static ModelT model(init.GetMoReFEMData());

            if (first_call)
            {
                model.Run();
                first_call = false;
            }

            return model;
        }



    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_HXX_
