/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace TestNcoordInDomainNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            full_mesh = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            volume_potential_1_potential_2 = 1,
            volume_displacement_potential_1 = 5,
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            potential_1 = 1,
            displacement = 5
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            potential_1 = 1,
            displacement_potential_1 = 5,
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : unsigned int
        {
            source = 1
        };


        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::potential_1)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestNcoordInDomainNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_PARAMETER_LIST_HPP_
