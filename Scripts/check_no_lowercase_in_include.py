import os

# MUST BE RUN IN THE SCRIPTS FOLDER!!!

if __name__ == '__main__':
    
    src_folder = '../Sources'
    ignore_dir_list = ['ThirdParty/Source', 'IntermediateBuild']
    
    # Run through all C++ files in src folder.
    for root, dirs, files in os.walk(src_folder):
        
        root_without_src_folder = root[len(src_folder)+1:]
        
        do_ignore_current_folder = False
        
        for dir_to_ignore in ignore_dir_list:
            if root_without_src_folder.startswith(dir_to_ignore):
                do_ignore_current_folder = True
                
        if do_ignore_current_folder:
            continue
        
        # Check no subfolder begins with lower case
        for mydir in dirs:
            if mydir and mydir[0].islower():
                print "{0}/{1} contains an unwanted lower case.".format(root_without_src_folder, mydir) 
        
        for file in files:

            if file.endswith(".cpp") or file.endswith(".hpp") or file.endswith(".hxx"):
                
                file_path = "{0}/{1}".format(root, file)
                
                # Check the file doesn't begin with lower caseself (except main files).
                if file[0].islower() and file[0:4] != "main":
                    print "{0} contains an unwanted lower case.".format(file_path)
                
                # Open each source file and look for includes with quotes.
                current_stream = open(file_path)
                
                for line in current_stream:
                    if line.startswith('#') and "include" in line and '<' not in line:
                        # Strip the line.
                        line = line.rstrip("\n")
                        
                        # Include with quotes should include a /: there are no files directly in Sources folder!
                        splitted = line.split("\"")
                        assert(len(splitted) > 1)
                        
                        path_in_include = splitted[1]
                        
                        # Track lower case: all after the first character or a '/' should be in lower case.
                        #print path_in_include    
                        if path_in_include.endswith('.h'):
                            continue                         
                            
                        #print path_in_include
                            
                        splitted = path_in_include.split('/')
                        
                        for item in splitted:
                            if not item:
                                print "[WARNING] // instead of / in path {0} in file {1}".format(path_in_include, file_path[len(src_folder)+1:])
                            
                            elif item[0].islower():
                                print "{0} in {1}".format(path_in_include, file_path[len(src_folder)+1:])
                                break
                        

                                
                                
        
        
        #print root, dirs, files