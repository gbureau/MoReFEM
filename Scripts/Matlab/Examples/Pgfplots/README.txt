This folder gives an example to create a pgfplot of 1D simulation with MoReFEM.

Command line : python create_gif.py Options/heat.py
	
Need image magick and pdftk, instructions to install them in create_gif.py.
	
The script will create a folder containing the .tex, .gif and the data files needed to create the ouput.

To adapt the script for your problem just create a new Option file in the folder and follow the instructions in it.

It is possible to plot multiple simulation on the same figure.
	
WARNING : It has been essentially tested for scalar variables. 
I do not guarantee a complete genericity but ont should take it as an example for create its own pgfplot.