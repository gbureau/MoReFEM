'load_example.m' is an example of how to load matrices, vectors and parameters that you can create with MoReFEM.

For obvious reasons MoReFEM should be run in Sequential.

Matrix file 'matrix.m' can be created within MoReFEM with:
matrix.View(GetMpi(), "PATH/matrix.m", __FILE__, __LINE__, PETSC_VIEWER_ASCII_MATLAB);

Vector file 'vector.hhdata' can be created within MoReFEM with: 
vector.Print<MpiScale::processor_wise>(GetMpi(), "PATH/vector.hhdata", __FILE__, __LINE__);

ScalarParameterAtQuadraturePoint file 'scalar_parameter_at_quadrature_point_parameter.dat' can be created within MoReFEM with:
GetParameter().Write("PATH/scalar_parameter_at_quadrature_point_parameter.dat");

'dofs_bc.dat' is a file containing the indices of the Boundary Condition in MoReFEM (carefull it is MoReFEM numbering and not the original mesh one). I can be created within MoReFEM with (Here for an example with the displacement unknown)

const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();
            
std::vector<unsigned int> dofs_bc;
            
for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
{
    assert(!(!dirichlet_bc_ptr));
                
    const auto& dirichlet_bc = *dirichlet_bc_ptr;
                
    const auto& dof_list = dirichlet_bc.GetDofList();
                
    int unsigned size = static_cast<unsigned int>(dof_list.size());
                
    for (unsigned int i = 0 ; i < size ; ++i)
        dofs_bc.push_back(dof_list[i]->GetProcessorWiseOrGhostIndex(displacement_numbering_subset));
}
            
std::ofstream outputFile ("PATH/dofs_bc.dat");
if (outputFile.is_open())
{
    for (unsigned int i = 0 ; i < dofs_bc.size() ; ++i)
    {
        outputFile << dofs_bc[i] << std::endl;
    }
    outputFile.close();
}

Energy file can be created in the same way as it is done in CardiacMechanicsPrestress.
