import os
import subprocess


def UpdateXCodeTemplate():
    
    print("=== Update XCode templates in user's Library. ===")
    morefem_folder = os.getcwd()
    
    current_folder = os.path.split(morefem_folder)[1]

    if current_folder != "MoReFEM":
        raise Exception("This script must be run from MoReFEM root folder!")
        
    target = os.path.join(os.path.expanduser("~"), "Library", "Developer", "Xcode", "Templates", "File Templates", "MoReFEM")
    
    if not os.path.exists(target):
         os.makedirs(target)
        
    # Same as target except for the handling of white space in File Templates. I didn't manage to handle it differently.
    target = target.replace(' ', '\ ')

    cmd = "rsync -v -r --delete {0} {1}".format(os.path.join(morefem_folder, "XCodeTemplates", "."), target)
    
    subprocess.Popen(cmd, shell = True).communicate()


if __name__ == "__main__":
    UpdateXCodeTemplate()