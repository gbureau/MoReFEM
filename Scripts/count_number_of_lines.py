import subprocess
import os


def ComputeCodeStats(morefem_src, output_directory):
    '''
    Compute the statistics about MoReFEM.
    \param morefem_src Path to the Sources/ directory of MoReFEM.
    \param output_directory Directory into which output and work files are written.
    '''
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    morefem_src = os.path.abspath(os.path.expanduser(morefem_src))
    
    cloc_exe = os.path.join(morefem_src, "ThirdParty", "Scripts", "cloc-1.70.pl")
    
    exclude_file = os.path.join(output_directory, "exclude_list.txt")
    
    FILE_exclude = open(exclude_file, 'w')
    FILE_exclude.write("{folder}\n".format(folder = os.path.join(morefem_src, "ThirdParty", "Source")))
    FILE_exclude.write("{folder}\n".format(folder = os.path.join(morefem_src, "ThirdParty", "Scripts")))
    FILE_exclude.write("{folder}\n".format(folder = os.path.join(morefem_src, "Test")))
    FILE_exclude.write("{folder}\n".format(folder = os.path.join(morefem_src, "ModelInstances")))    
    FILE_exclude.close()
    
    cmd = (cloc_exe, \
           morefem_src, \
           "--force-lang=C++,hxx", \
           "--force-lang=C++,doxygen", \
           '--exclude-list-file', \
            exclude_file, \
            '--ignored', \
            os.path.join(output_directory, 'ignored.txt'), \
            '--counted', \
            os.path.join(output_directory, 'files_considered.txt'), \
            '--report-file', \
            os.path.join(output_directory, 'report.txt'), \
           )
    
    
    print(" ".join(cmd))

    subprocess.Popen(' '.join(cmd), shell = True).communicate()



if __name__ == "__main__":    
    ComputeCodeStats('~/Codes/MoReFEM/Sources', '/Volumes/Data/sebastien/MoReFEM/Stats')
