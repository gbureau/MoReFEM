class Target(object):
    """Object which handles the executable to compile and check and the Lua file to use in those tests."""
    
    def __init__(self, target_name, lua_file_list, do_call_ensight_output = True, Nproc = 4, is_sequential = True):
        """
        \param[in] target_name Name of the executable.
        \param[in] lua_file_list List of Lua files to run against the executable. The path to each of this file is given relatively to the root of MoReFEM directory.
        \param[in] do_call_ensight_output True if Ensight output is meaningfull for the target. Usually True for demo 
        ModelInstance and False for integration tests.
        \param[in] Nproc Number of processors to deal with in parallel. Put 0 or 1 if parallel is not relevant for the target.        
        \param[in] is_sequential For some tests related to parallel behaviour, sequential run is meaningless.
        """
        self.executable = target_name
        self.lua_file_list = lua_file_list
        self.do_call_ensight_output = do_call_ensight_output
        self.Nproc = Nproc
        self.is_sequential = is_sequential
        
        
class TestTarget(Target):
    
    def __init__(self, target_name, lua_file_list, Nproc = 4, is_sequential = True):    
    
        super(TestTarget, self).__init__(target_name,
                                         lua_file_list,
                                         False,
                                         Nproc,
                                         is_sequential)
        


def TestTargetList():
    """List of all tests to run in the integration process."""
    

    vertex_matching_operator_lua_list = \
('Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_scalar_P1.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P1.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P1b.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P2.lua')

    # IMPORTANT:
    # test_mpi_error_handling is ignored for the time being, as it is a test designed to fail.
    # Add it properly in the upcoming continuous integration test!
    
    return (\
    TestTarget('MoReFEMTestInputParameterList', \
           ('Sources/Test/Utilities/InputParameterList/demo_input_parameter_test_ipl.lua', )),
    TestTarget('MoReFEMTestMpiFunctions', \
            ('Sources/Test/ThirdParty/Mpi/Functions/demo_input_parameter_mpi_functions.lua', ),
            is_sequential = False
            ),
    TestTarget('MoReFEMTestMpiSendReceiveArray', \
            ('Sources/Test/ThirdParty/Mpi/SendReceive/demo_input_parameter_mpi_send_receive.lua', ),
            is_sequential = False
            ),            
    TestTarget('MoReFEMTestMpiSendReceiveSingleValue', \
            ('Sources/Test/ThirdParty/Mpi/SendReceive/demo_input_parameter_mpi_send_receive.lua', ),
            is_sequential = False
            ),              
    TestTarget('MoReFEMTestOndomatic', \
           ('Sources/Test/Ondomatic/demo_input_ondomatic_2D.lua',
            'Sources/Test/Ondomatic/demo_input_ondomatic_3D.lua')),
    TestTarget('MoReFEMTestDomainListInCoords', \
           ('Sources/Test/Geometry/DomainListInCoords/demo_input_test_domain_list_in_coords.lua', )),
    TestTarget('MoReFEMTestLightweightDomainList', \
           ('Sources/Test/Geometry/LightweightDomainList/demo_input_test_lightweight_domain_list.lua', )),
    TestTarget('MoReFEMTestColoring', \
           ('Sources/Test/Geometry/Coloring/demo_input_test_coloring.lua', )),
    TestTarget('MoReFEMTestCoordsInParallel', \
           ('Sources/Test/Geometry/CoordsInParallel/demo_input_test_coords_in_parallel.lua', ),
           is_sequential = False),
    TestTarget('MoReFEMTestMovemesh', \
           ('Sources/Test/Geometry/Movemesh/demo_input_parameter_movemesh.lua', )),           
    TestTarget('MoReFEMTestP1_to_P2', \
           ('Sources/Test/Operators/P1_to_HigherOrder/demo_input_test_P1_to_P2.lua', )),
    TestTarget('MoReFEMTestP1_to_P1b', \
           ('Sources/Test/Operators/P1_to_HigherOrder/demo_input_test_P1_to_P1b.lua', )),
    TestTarget('MoReFEMTestConformProjector', \
           ('Sources/Test/Operators/ConformProjector/demo_input_test_conform_projector.lua', )),
    TestTarget('MoReFEMTestFromVertexMatching', vertex_matching_operator_lua_list),
    # TestTarget('test_param_at_dof', \
    #        ('Sources/Test/Parameter/AtDof/demo_input_test_at_dof_parameter.lua', )),
    TestTarget('MoReFEMTestParameterTimeDependancy', \
           ('Sources/Test/Parameter/TimeDependency/demo_input_test_parameter_time_dependency.lua', ),
            Nproc = 0),
    TestTarget('MoReFEMTestOperatorTestFunctions', None, Nproc = 0)

    )
    
    return ret
    

def ModelTargetList():
    """List of all the demonstration model instances to run in the integration process."""
    
    return (\
    Target('MoReFEM4Stokes', \
          ('Sources/ModelInstances/Stokes/demo_input_stokes.lua', ),
          do_call_ensight_output = False), \
    Target('MoReFEM4Elasticity', \
          ('Sources/ModelInstances/Elasticity/demo_input_elasticity.lua', \
           'Sources/ModelInstances/Elasticity/demo_input_elasticity_3d.lua' )), \
    Target('MoReFEM4Hyperelasticity', \
          ('Sources/ModelInstances/Hyperelasticity/demo_input_hyperelasticity.lua', )), \
    Target('MoReFEM4Heat', \
          ('Sources/ModelInstances/Heat/demo_input_heat.lua', \
           'Sources/ModelInstances/Heat/demo_input_heat_1d.lua')), \
    Target('MoReFEM4RivlinCube', \
          ('Sources/ModelInstances/RivlinCube/demo_input_rivlin_cube_hexahedra.lua', \
           'Sources/ModelInstances/RivlinCube/demo_input_rivlin_cube_tetrahedra.lua')), \
    Target('MoReFEM4Laplacian', \
          ('Sources/ModelInstances/Laplacian/demo_input_laplacian.lua', )), \

    )
    
    
    
def TestAndModelTargetList():
    """List of all the tests and all the demonstration model instances to run in the integration process."""
    
    ret = list(TestTargetList())
    ret.extend(ModelTargetList())
       
    return ret
    
   
    
