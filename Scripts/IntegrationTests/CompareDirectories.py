import os
import filecmp


def ExtractFilesThatDiffer(dcmp, original_left, startswith_ignore_list = None):
    """
    \param[in] dcmp Python dircmp object.
    \param[in] startswith_ignore_list If a filename starts by one of these elements, it won't be reported as a file that differs. It's a hack because I didn't want to bother at thew moment with regular expression; I wanted to be
    able to silence time_log.*.hhdata.
    """
    diff_files = []
    
    relative_path = dcmp.left[len(original_left) + 1:]

    for name in dcmp.diff_files:
        
        skip_file = False
        
        if startswith_ignore_list:
            for startswith_items in startswith_ignore_list:
                if name.startswith(startswith_items):
                    skip_file = True
                    
        if not skip_file:
            diff_files.append(os.path.join(relative_path, name))

    for sub_dcmp in dcmp.subdirs.values():
        diff_files.extend(ExtractFilesThatDiffer(sub_dcmp, original_left, startswith_ignore_list = startswith_ignore_list))
        
    return diff_files


def ExtractLeftOnlyFiles(dcmp):
    """
    \param[in] dcmp Python dircmp object.
    """
    left_only_files = []
    
    for name in dcmp.left_only:
        left_only_files.append(os.path.join(dcmp.left, name))

    for sub_dcmp in dcmp.subdirs.values():
        left_only_files.extend(ExtractLeftOnlyFiles(sub_dcmp))
        
    return left_only_files


def ExtractRightOnlyFiles(dcmp):
    """
    \param[in] dcmp Python dircmp object.
    """
    right_only_files = []
    
    for name in dcmp.right_only:
        right_only_files.append(os.path.join(dcmp.right, name))

    for sub_dcmp in dcmp.subdirs.values():
        right_only_files.extend(ExtractRightOnlyFiles(sub_dcmp))
        
    return right_only_files
    
    

class CompareDirectories:


    """
    \param[in] left One of the directory to compare.
    \param[in] right The other directory being compared.
    \param[in] startswith_ignore_list If a filename starts by one of these elements, it won't be reported as a file that differs. It's a hack because I didn't want to bother at thew moment with regular expression; I wanted to be
    able to silence time_log.*.hhdata.
    \param[in] preffix Preffix to add at each line printed; e.g. '\t'.
    """
    def __init__(self, left, right, startswith_ignore_list = None, preffix = ''):

        self.__internal = filecmp.dircmp(left, right)
        
        self.__diff_files = ExtractFilesThatDiffer(self.__internal, original_left = left, startswith_ignore_list = startswith_ignore_list)
        self.__left_only = ExtractLeftOnlyFiles(self.__internal)
        self.__right_only = ExtractRightOnlyFiles(self.__internal)
        self.__preffix = preffix
                
    
    def AreIdentical(self):
        """Whether the folder are strictly identical or not."""
        
        return not self.__diff_files and not self.__left_only and not self.__left_only
        
        
    def __str__(self):
        if self.AreIdentical():
            return "{0}Identical directories.".format(self.__preffix)
            
        else:
            ret = ""
            if self.__diff_files:
                ret = "{0}The following files differ:\n".format(self.__preffix)
                
                for file in self.__diff_files:
                    ret += "{0}\t- {1}\n".format(self.__preffix, file)
                    
            if self.__left_only:
                ret += "{0}The following files are only present in {left} but not in {right}:\n".format(self.__preffix, left = self.__internal.left, right = self.__internal.right)
                
                for file in self.__left_only:
                    ret += "{0}\t- {1}\n".format(self.__preffix, file)
                    
            if self.__right_only:
                ret += "{0}The following files are only present in {right} but not in {left}:\n".format(self.__preffix, left = self.__internal.left, right = self.__internal.right)
                
                for file in self.__right_only:
                    ret += "{0}\t- {1}\n".format(self.__preffix, file)                    
                
            return ret 
            
